﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;


public class ObjetoInfo : MonoBehaviour
{
    public ObjetoInteract Info;
    public bool usado;
    public static int contador;
    public static int _cuentaNecesaria;

    // Start is called before the first frame update
    void Start()
    {
        usado = false;
        contador = 0;
        _cuentaNecesaria = 7;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void USADODone()
    {
        usado = true;
        contador++;
    }
}
