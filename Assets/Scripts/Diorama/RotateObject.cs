﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RotateObject : MonoBehaviour
{
    protected Vector3 posLastFrame;
    public GameObject Son;
    DioramaManager manager;

    private void Start()
    {
        manager = FindObjectOfType<DioramaManager>();
    }

    private void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0) && manager.pause==false)
        {
            posLastFrame = Input.mousePosition;
        }

        if (Input.GetMouseButton(0) && manager.pause == false)
        {
            if (Son!=null)
            {
                var pos = Input.mousePosition - posLastFrame;
                posLastFrame = Input.mousePosition;

                var objpos = Quaternion.AngleAxis(-90f, Vector3.forward) * pos;
                Son.transform.rotation = Quaternion.AngleAxis(pos.magnitude * 01f, objpos) * Son.transform.rotation;
            }
        }
    }

}
