﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScreenDescription : MonoBehaviour
{
    [Header("Objectos")]
    [SerializeField]
    private MouseLook Player;
    [SerializeField]
    private RotateObject objeto;
    [SerializeField]
    private PLayerMovement PlayerMov;
    [SerializeField]
    private GameObject vista;
    GameObject Xd;

    [Header("Interface")]
    [SerializeField]
    private Text _name;
    [SerializeField]
    private Text _Description;
    [SerializeField]
    private Image _Imagen;
    public ObjetoInteract Objeto;
    [SerializeField]
    private Transform Posicion;
    DioramaManager manager;
    AudioManager Z;

    private void Awake()
    {
        manager = FindObjectOfType<DioramaManager>();
        Z = FindObjectOfType<AudioManager>();
    }
    void Update()
    {
        if (this.gameObject.activeInHierarchy==true)
        {
            vista.SetActive(false);
            PlayerMov.enabled = false;
            Player.enabled = false;
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;

            if (Input.GetKeyDown(KeyCode.E) && manager.pause==false)
            {
                Close();
                if (Objeto.sword==false)
                {
                    manager.DescNorm();
                }
                else
                {
                    manager.DescEsp();
                    manager.EndSword();
                }
            }
        }
    }

    public void Close()
    {
        Z.Play("DescriptionCerrar");
        Z.PlayItems();
        vista.SetActive(true);
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        PlayerMov.enabled = true;
        Player.enabled = true;
        deletetext();
        Destroy(Xd);
        this.gameObject.SetActive(false);
    }

    void deletetext()
    {
        _name.text = "";
        _Description.text = "";
    }

    public void Datos()
    {
        _name.text = Objeto.Name;
        _Description.text = Objeto.Descripcion;
        _Imagen.sprite=Objeto.sprite;
        if (Xd==null)
        {
            Xd = Instantiate(Objeto.Objeto3DObservar, Posicion);
        }
        objeto.Son = Xd.transform.GetChild(0).transform.gameObject;
    }
}
