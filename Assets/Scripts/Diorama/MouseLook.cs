﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MouseLook : MonoBehaviour
{
    [SerializeField]
    private float mouseSensi;
    [SerializeField]
    private float RaycastDista;
    private Transform PlayerBody;
    [SerializeField]
    private LayerMask Interact;
    [SerializeField]
    private GameObject InteractShow;
    private Text toInteract;
    [SerializeField]
    private GameObject InteractScreen;
    private ScreenDescription MenuDes;
    private float xrotation;
    private DioramaManager manager;
    AudioManager Z;
    void Start()
    {
        manager = FindObjectOfType<DioramaManager>();
        InteractShow.SetActive(false);
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        PlayerBody = this.transform.parent.transform;
        MenuDes = InteractScreen.GetComponent<ScreenDescription>();
        toInteract = InteractShow.transform.GetChild(1).GetComponent<Text>();
        Z = FindObjectOfType<AudioManager>();
    }

    void Update()
    {
        movCam();
        if (manager.intetrue)
        {
            castRay(this.transform.position, transform.forward);
        }
        else
        {
            InteractShow.SetActive(false);
        }
    }

    private void movCam()
    {
        float mouseX = Input.GetAxis("Mouse X") * mouseSensi * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensi * Time.deltaTime;

        xrotation -= mouseY;
        xrotation = Mathf.Clamp(xrotation, -90f, 90f);

        transform.localRotation = Quaternion.Euler(xrotation, 0f, 0f);
        PlayerBody.Rotate(Vector3.up * mouseX);
    }

    void castRay(Vector3 position, Vector3 direction)
    {
        RaycastHit hit;
        Debug.DrawRay(position, direction * RaycastDista, Color.red);

        if (Physics.Raycast(position,direction,out hit,RaycastDista,Interact))
        {
            ObjetoInfo X = hit.transform.gameObject.GetComponent<ObjetoInfo>();
            ChangeInteractTo(X);
            InteractShow.SetActive(true);
            if (Input.GetKeyDown(KeyCode.E) && X.Info.sword==false && manager.pause==false)
            {
                if (hit.transform.gameObject.GetComponent<NaviHey>()!=null)
                {
                    hit.transform.gameObject.GetComponent<NaviHey>().Play();
                }
                Z.Play("DescriptionAbrir");
                Z.PauseItems();
                InteractScreen.SetActive(true);
                manager.DescNorm();
                if (X.usado!=true)
                {
                    X.USADODone();
                }
                MenuDes.Objeto = X.Info;
                MenuDes.Datos();
            }
            else if (Input.GetKeyDown(KeyCode.E) && X.Info.sword == true && ObjetoInfo.contador >= ObjetoInfo._cuentaNecesaria && manager.pause == false)
            {
                Z.Play("DescriptionAbrir");
                Z.PauseItems();
                InteractScreen.SetActive(true);
                manager.DescEsp();
                if (X.usado != true)
                {
                    X.USADODone();
                }
                MenuDes.Objeto = X.Info;
                MenuDes.Datos();
            }
        }
        else
        {
            InteractShow.SetActive(false);
        }
    }

    void ChangeInteractTo(ObjetoInfo X)
    {
        if (X.Info.sword == false)
        {
            toInteract.text = "To Interact";
        }
        else
        {
            if (ObjetoInfo.contador < ObjetoInfo._cuentaNecesaria)
            {
                toInteract.text = "Need " + ObjetoInfo.contador + "/" + ObjetoInfo._cuentaNecesaria;
            }
            else
            {
                toInteract.text = "To Interact";
            }
        }
    }
}
