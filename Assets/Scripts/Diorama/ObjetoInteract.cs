﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName ="Inteactuable",menuName = "ObjetosInteractuables")]
public class ObjetoInteract : ScriptableObject
{   
    public string Name;
    [TextArea(1,5)]
    public string Descripcion;
    public GameObject Objeto3DObservar;
    public Sprite sprite;
    public bool sword;
}
