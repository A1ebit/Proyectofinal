﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class DioramaManager : MonoBehaviour
{
    [SerializeField]
    public bool pause;
    [SerializeField]
    private bool DescriptionNormal;
    [SerializeField]
    private bool DescriptionSword;  
    [SerializeField]
    private GameObject PausaMenu;
    AudioManager Z;
    public AudioMixer Mixer;
    Animator X;
    public bool intetrue;

    [Header("EspadaFinal")]
    [SerializeField]
    GameObject Sword;    
    [SerializeField]
    Animator HojaSword; 

    void Start()
    {
        intetrue = true;
        X = GameObject.Find("Transition").GetComponent<Animator>();
        X.Play("trancision");
        StartCoroutine(FadeMixerGroup.StartFade(Mixer, "Volume", 3, 1));
        pause = false;
        DescriptionNormal = false;
        DescriptionSword = false;
        Z = FindObjectOfType<AudioManager>();
        Z.Play("Theme");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            pause = !pause;

            if (pause)
            {
                Z.PauseItemsManager();
                Z.PauseItems();
                Z.Play("PausaAbrir");
            }
            else
            {
                Z.Play("PausaCerra");
                Z.PlayItemsManager();
                Z.PlayItems();
            }
        }     
        PausaMenuFuntion();
        if (Input.GetKeyDown(KeyCode.P))
        {
            EndSword();
        }
    }

    public void Pausa()
    {
        pause = !pause;
        Z.Play("PausaCerra");
        Z.PlayItemsManager();
        Z.PlayItems();
    }
    public void DescNorm()
    {
        DescriptionNormal = !DescriptionNormal;
    }   
    public void DescEsp()
    {
        DescriptionSword = !DescriptionSword;
    }

    void PausaMenuFuntion()
    {
        if (pause)
        {
            PausaMenu.SetActive(true);
            Time.timeScale = 0;
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
        else if ((DescriptionNormal==false||DescriptionSword==false) && pause==false)
        {
            PausaMenu.SetActive(false);
            Time.timeScale = 1;
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }
    }

    public void quit()
    {
        Application.Quit();
    } 
    
    public void EndSword()
    {
        intetrue = false;
        Z.Play("Master");
        StartCoroutine(SwordEndMove(Sword, Sword.transform.position + new Vector3(0, 0.148486f, 0), .4f));
    }

    IEnumerator SwordEndMove(GameObject A,Vector3 B, float speed)
    {
        yield return new WaitForSeconds(1.85f);
        HojaSword.Play("ChangeColor");
        while (A.transform.position != B)
        {
            A.transform.position = Vector3.MoveTowards(A.transform.position/*Vector3.Lerp(A.transform.position*/, /*B, Time.deltaTime), B*/Vector3.Lerp(A.transform.position, B, Time.deltaTime*(speed+1f)), speed*Time.deltaTime);
            yield return null;
        }
        yield return new WaitForSeconds(0.45f);
        StartCoroutine(FadeMixerGroup.StartFade(Mixer, "Volume", 2.75f, 0));
        X.Play("trancisionCredits");
        sceneControles.NextSceneNum(5);
        yield return new WaitForSeconds(3.5f);
        sceneControles.ChangeScene();
    }
}
