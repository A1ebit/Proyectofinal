﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightHada : MonoBehaviour
{
    [SerializeField]
    private Light myLight;   
    private bool ChangeInten=true;
    [SerializeField]
    private float IntensitySpeed;
    private float MaxIntensity;

    // Start is called before the first frame update
    void Start()
    {
        myLight = GetComponent<Light>();
        MaxIntensity = myLight.intensity;
    }

    // Update is called once per frame
    void Update()
    {
        if (ChangeInten)
        {
            myLight.intensity = Mathf.PingPong(Time.time * IntensitySpeed, MaxIntensity);
        }
    }
}
