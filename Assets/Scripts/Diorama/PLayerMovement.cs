﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PLayerMovement : MonoBehaviour
{
    private CharacterController PlayerCuco;
    public float speed;
    public float gravity = -9.81f;
    Vector3 velocity;
    public Transform groundcheck;
    public float ratio=0.4f;
    public float Jump=1f;
    public LayerMask ground;
    private bool isground;

    void Start()
    {
        PlayerCuco = GetComponent<CharacterController>();
    }


    void Update()
    {
        isground = Physics.CheckSphere(groundcheck.position, ratio, ground);
        if (isground && velocity.y<0)
        {
            velocity.y = -2f;
        }
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 move = transform.right*x+transform.forward*z;

        PlayerCuco.Move(move*speed*Time.deltaTime);

        velocity.y += gravity * Time.deltaTime;

        PlayerCuco.Move(velocity*Time.deltaTime);

        if (Input.GetKeyDown(KeyCode.Space) && isground)
        {
            velocity.y = Mathf.Sqrt(Jump * -2f * gravity);
        }
    }
}
