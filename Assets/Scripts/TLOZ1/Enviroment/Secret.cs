﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Secret : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Bomb"))
        {
            StartCoroutine(Secrt());
            //this.gameObject.GetComponent<BoxCollider2D>().enabled = false;
            //this.gameObject.GetComponent<SpriteRenderer>().enabled = true;
        }
    }

    IEnumerator Secrt()
    {
        FindObjectOfType<AudioManager>().Play("Secret");
        yield return new WaitForSeconds(0.35f);
        this.gameObject.GetComponent<BoxCollider2D>().enabled = false;
        this.gameObject.GetComponent<SpriteRenderer>().enabled = true;
    }

}
