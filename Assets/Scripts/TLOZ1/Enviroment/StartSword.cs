﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartSword : MonoBehaviour
{
    public Text Texto;
    public bool rep=true;
    public string sentence;
    public GameObject espada;
    public Transform spawnPosition;
    public bool inicio;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (inicio==true)
        {
            GameObject dc = GameObject.Find("/CambiosCollider/(1-2)/Cueva_Fuera");
            dc.GetComponent<BoxCollider2D>().enabled = false;
            if (other.gameObject.CompareTag("Player") && rep==true)
            {
                rep = false;
                StartCoroutine(escribirinicio());
                Debug.Log("AYUDA");
            }
        }
        else
        {
            GameObject dc = GameObject.Find("/CambiosCollider/(5-Secret)/Derecha");
            dc.GetComponent<BoxCollider2D>().enabled = false;
            if (other.gameObject.CompareTag("Player") && rep == true)
            {
                rep = false;
                StartCoroutine(escribir());
                Debug.Log("Hello there");
            }
        }
    }

    IEnumerator escribir()
    {
        FindObjectOfType<AudioManager>().Play("TextoSlow");
        Texto.text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            Texto.text += letter;
            yield return new WaitForSeconds(0.125f);
        }
        FindObjectOfType<AudioManager>().Stop("TextoSlow");
    }
    IEnumerator escribirinicio()
    {
        FindObjectOfType<AudioManager>().Play("TextoSlow");
        Texto.text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            Texto.text += letter;
            yield return new WaitForSeconds(0.125f);
        }
        FindObjectOfType<AudioManager>().Stop("TextoSlow");
        Instantiate(espada, spawnPosition.position, spawnPosition.rotation);
    }
}
