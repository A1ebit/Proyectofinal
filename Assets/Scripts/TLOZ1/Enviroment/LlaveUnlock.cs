﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LlaveUnlock : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Manager ca = GameObject.Find("GameManager").GetComponent<Manager>();
            if (ca.Llave >= 1) 
            {
                FindObjectOfType<AudioManager>().Play("DoorUnlock");
                Destroy(this.gameObject,0.5f);
                ca.Llave--;
            }
            else
            {
            Debug.Log("necesito llave");
            }
        }
    }
}
