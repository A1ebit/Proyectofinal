﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TirgersCamara : MonoBehaviour
{
    public Collider2D col;
    public GameObject cam;
    [HideInInspector]
    public GameObject player;
    public PlayerControler LinkOg;
    [HideInInspector]
    public GameObject location;
    [HideInInspector]
    public GameObject renders;
    //[HideInInspector]
    public Animator anim;


    public bool camera;
    public bool up;
    public bool down;
    public bool right;
    public bool left;
    public bool cave;
    public bool hide;
    private void Awake()
    {
        col = this.GetComponent<Collider2D>();
        cam = GameObject.Find("Main Camera");
        player = GameObject.Find("Link");
        LinkOg = player.GetComponent<PlayerControler>();
        location = GameObject.Find("LinkLocation");
        renders = GameObject.Find("Personajes");
        anim = GameObject.Find("Transicion").GetComponent<Animator>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (cave)
            {
                FindObjectOfType<AudioManager>().Play("Cueva");
            }
            if (camera==true)
            {
                StartCoroutine(CameraMove());
            }
            else if (hide==true)
            {
                player.GetComponent<SpriteRenderer>().enabled = false;
            }
            Debug.Log(cam.transform.position);
            Debug.Log("si funciono");
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player") || collision.gameObject.CompareTag("NPC"))
        {
            if (hide == true)
            {
                player.GetComponent<SpriteRenderer>().enabled = true;
            }
        }
    }

    IEnumerator CameraMove()
    {
        renders.SetActive(false);
        LinkOg.Invencible = false;
        player.GetComponent<SpriteRenderer>().color=Color.white;
        if (down==true)
        {
            if (cave == true)
            {
                anim.Play("Transicion");
                for (int i = 0; i <= 46; i++)
                {
                    yield return new WaitForSeconds(0.02f);
                    cam.transform.position = new Vector2(cam.transform.position.x, cam.transform.position.y - 2.81f);
                }
                location.transform.position = new Vector3(location.transform.position.x, location.transform.position.y - 5.3f, location.transform.position.z);
                player.transform.position = new Vector3(player.transform.position.x, player.transform.position.y - 24f, player.transform.position.z);
            }
            else
            {

                for (int i = 0; i <= 46; i++)
                {
                    yield return new WaitForSeconds(0.025f);
                    cam.transform.position = new Vector2(cam.transform.position.x, cam.transform.position.y - 2.81f);
                }
                location.transform.position = new Vector3(location.transform.position.x, location.transform.position.y - 5.3f, location.transform.position.z);
                player.transform.position = new Vector3(player.transform.position.x, player.transform.position.y - 15f, player.transform.position.z);
            }
        }
        else if (up == true)
        {
            if (cave == true)
            {
                anim.Play("Transicion");

                for (int i = 0; i <= 46; i++)
                {
                    yield return new WaitForSeconds(0.02f);
                    cam.transform.position = new Vector2(cam.transform.position.x, cam.transform.position.y + 2.81f);
                }
                location.transform.position = new Vector3(location.transform.position.x, location.transform.position.y + 5.3f, location.transform.position.z);
                player.transform.position = new Vector3(player.transform.position.x, player.transform.position.y + 24f, player.transform.position.z);
            }
            else
            {
                for (int i = 0; i <= 46; i++)
                {
                    yield return new WaitForSeconds(0.025f);
                    cam.transform.position = new Vector2(cam.transform.position.x, cam.transform.position.y + 2.81f);
                }
                location.transform.position = new Vector3(location.transform.position.x, location.transform.position.y + 5.3f, location.transform.position.z);
                player.transform.position = new Vector3(player.transform.position.x, player.transform.position.y + 15f, player.transform.position.z);
            }
        }
        else if (left == true)
        {
            if (cave==true)
            {
                anim.Play("Transicion");
                player.transform.position = new Vector3(player.transform.position.x - 14f, player.transform.position.y, player.transform.position.z + 0);

            }
            for (int i = 0; i <= 46; i++)
            {
                yield return new WaitForSeconds(0.025f);
                cam.transform.position = new Vector2(cam.transform.position.x - 6.005f, cam.transform.position.y);
            }
            location.transform.position = new Vector3(location.transform.position.x - 15.2f, location.transform.position.y, location.transform.position.z);
            player.transform.position = new Vector3(player.transform.position.x - 16f, player.transform.position.y, player.transform.position.z +0);
        }
        else if (right == true)
        {
            if (cave == true)
            {
                anim.Play("Transicion");
                player.transform.position = new Vector3(player.transform.position.x + 14f, player.transform.position.y, player.transform.position.z + 0);

            }
            for (int i = 0; i <= 46; i++)
            {
                yield return new WaitForSeconds(0.025f);
                cam.transform.position = new Vector2(cam.transform.position.x + 6.005f, cam.transform.position.y);
            }
            location.transform.position = new Vector3(location.transform.position.x + 15.2f, location.transform.position.y, location.transform.position.z);
            player.transform.position = new Vector3(player.transform.position.x + 16f, player.transform.position.y, player.transform.position.z + 0);
        }

        yield return new WaitForSeconds(0.15f);
        renders.SetActive(true);
    }

}
