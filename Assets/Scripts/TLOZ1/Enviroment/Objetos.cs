﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Objetos : MonoBehaviour
{
    [HideInInspector]
    public GameObject player;
    public bool ContainerHearth;
    public bool Llave;
    public bool bomb;
    public bool Espada;
    public bool Rupia;
    public bool RupiaS;
    public bool Corazon;
    AudioManager Audi;

    private void Start()
    {
        player = GameObject.Find("Link");
        Audi = FindObjectOfType<AudioManager>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (ContainerHearth == true)
            {
                StartCoroutine(heart());
            }
            else if (Llave== true)
            {
                StartCoroutine(key());
            }
            else if (bomb == true)
            {
                StartCoroutine(bomba());
            }            
            else if (Espada == true)
            {
                StartCoroutine(espada());
            } 
            else if (Rupia == true)
            {
                StartCoroutine(rupia());
            }    
            else if (RupiaS == true)
            {
                StartCoroutine(rupiaS());
            }     
            else if (Corazon == true)
            {
                StartCoroutine(Vida());
            }
        }
    }

    IEnumerator heart()
    {
        Debug.Log("si mas vida");
        Animator dan = player.GetComponent<Animator>();
        Vector2 ka = player.transform.position;
        player.GetComponent<PlayerControler>().atke = true;
        player.GetComponent<BoxCollider2D>().enabled = false;
        this.gameObject.GetComponent<Collider2D>().enabled = false;
        Audi.Play("Item");
        dan.Play("Recogido");
        player.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        this.gameObject.transform.position = new Vector3(player.transform.position.x, player.transform.position.y + 9.5f, player.transform.position.z);
        yield return new WaitForSeconds(1.25f);
        PlayerControler ca = player.GetComponent<PlayerControler>();
        ca.more();
        player.GetComponent<PlayerControler>().atke = false;
        player.GetComponent<BoxCollider2D>().enabled = true;
        dan.Play("Player");
        Destroy(this.gameObject);
    }    

    IEnumerator espada()
    {
        Debug.Log("si una espada");
        Animator dan = player.GetComponent<Animator>();
        Vector2 ka = player.transform.position;
        player.GetComponent<PlayerControler>().atke = true;
        player.GetComponent<BoxCollider2D>().enabled = false;
        this.gameObject.GetComponent<Collider2D>().enabled = false;
        Audi.Play("SwordMaster");
        dan.Play("Recogido");
        player.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        this.gameObject.transform.position = new Vector3(player.transform.position.x, player.transform.position.y + 9.5f, player.transform.position.z);
        yield return new WaitForSeconds(1.25f);
        Manager ca = GameObject.Find("GameManager").GetComponent<Manager>();
        player.GetComponent<PlayerControler>().atke = false;
        player.GetComponent<BoxCollider2D>().enabled = true;
        dan.Play("Player");
        GameObject us = GameObject.Find("TextEdit");
        GameObject ek = GameObject.Find("StartTalk");
        GameObject dc = GameObject.Find("/Canvas/UI/Marcos/EspadaMarco");
        Destroy(this.gameObject);
        Destroy(ek);
        Text ac = us.GetComponent<Text>();
        ac.text = "";
        dc.SetActive(true);
        GameObject hg = GameObject.Find("/CambiosCollider/(1-2)/Cueva_Fuera");
        hg.GetComponent<BoxCollider2D>().enabled = true;
        ca.espada = true;
    }

    IEnumerator key()
    {
        Debug.Log("si mas llaves");
        Animator dan = player.GetComponent<Animator>();
        Vector2 ka = player.transform.position;
        player.GetComponent<PlayerControler>().atke = true;
        player.GetComponent<BoxCollider2D>().enabled = false;
        this.gameObject.GetComponent<Collider2D>().enabled = false;
        Audi.Play("Item");
        dan.Play("Recogido");
        player.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        this.gameObject.transform.position = new Vector3(player.transform.position.x, player.transform.position.y + 9.5f, player.transform.position.z);
        yield return new WaitForSeconds(1.25f);
        Manager ca = GameObject.Find("GameManager").GetComponent<Manager>();
        ca.Llave++;
        player.GetComponent<PlayerControler>().atke = false;
        player.GetComponent<BoxCollider2D>().enabled = true;
        dan.Play("Player");
        Destroy(this.gameObject);
    }    

    IEnumerator bomba()
    {
        Debug.Log("si mas bombas");
        Audi.Play("Heart");
        yield return new WaitForSeconds(0.1f);
        Manager ca = GameObject.Find("GameManager").GetComponent<Manager>();
        ca.Bombs++;
        Destroy(this.gameObject);
    }   
    
    IEnumerator Vida()
    {
        Debug.Log("si mas vida pa mi");
        yield return new WaitForSeconds(0.1f);
        PlayerControler ca = GameObject.Find("Link").GetComponent<PlayerControler>();
        if (ca.CurrentHealt==ca.MaxHealt)
        {
            Manager dc = GameObject.Find("GameManager").GetComponent<Manager>();
            dc.Rupias++;
            Audi.Play("Rupee");
        }
        else
        {
            Audi.Play("Heart");
            ca.CurrentHealt++;
        }
        Destroy(this.gameObject);
    }   
    
    IEnumerator rupia()
    {
        Debug.Log("si mas money");
        yield return new WaitForSeconds(0.1f);
        Manager ca = GameObject.Find("GameManager").GetComponent<Manager>();
        Audi.Play("Rupee");
        ca.Rupias++;
        Destroy(this.gameObject);
    }   

    IEnumerator rupiaS()
    {
        Debug.Log("si mas moneycsecret");
        yield return new WaitForSeconds(0.1f);
        Manager ca = GameObject.Find("GameManager").GetComponent<Manager>();
        ca.Rupias+=10;
        Audi.Play("Rupee");
        GameObject dc = GameObject.Find("/CambiosCollider/(5-Secret)/Derecha");
        GameObject us = GameObject.Find("TextEdit");
        GameObject ek = GameObject.Find("StartTalkS");
        Destroy(ek);
        Text ac = us.GetComponent<Text>();
        ac.text = "";
        dc.GetComponent<BoxCollider2D>().enabled = true;
        Destroy(this.gameObject);
    }
}
