﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lanzar : MonoBehaviour
{

    public float speed = 20f;
    public Rigidbody2D rb;
    Transform sw;
    public bool espada;
    public bool Zora;
    PlayerControler Link;
    Vector3 move;
    void Start()
    {
        if (Zora==true)
        {
            Link = GameObject.FindObjectOfType<PlayerControler>();
            move = (Link.transform.position - transform.position).normalized * speed;
            rb.velocity = new Vector3(move.x, move.y, move.z);
            speed = 30f;
        }
        else
        {
            rb.velocity = transform.up * speed;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (espada == true)
        {
            if (collision.tag=="Enviroment" || collision.tag == "Enemy" || collision.tag == "NPC")
            {
                Destroy(this.gameObject);
            }
        }
        else
        {
            if (collision.tag == "Enviroment" || collision.tag == "NPC" || collision.tag== "Shield")
            {
                if (collision.tag== "Shield")
                {
                    FindObjectOfType<AudioManager>().Play("Shield");
                }
                Debug.Log("BetterLuckNectTime");
                Destroy(this.gameObject);
            }
            else if (collision.tag == "Player")
            {
                GameObject yc = GameObject.Find("Link");
                if (yc.GetComponent<PlayerControler>().Invencible==false)
                {
                    yc.GetComponent<PlayerControler>().damage();
                }
                Destroy(this.gameObject);
            }
        }

    }

}
