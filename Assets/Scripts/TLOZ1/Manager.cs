﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class Manager : MonoBehaviour
{
    public Text NumRupias;
    public Text NumLlaves;
    public Text NumBombs;
    public int Bombs;
    public int Llave;
    public int Rupias;
    public bool espada;
    [SerializeField]
    bool _IsPaused;  
    [SerializeField]
    GameObject _PausedMenu; 
    AudioManager Z;
    public AudioMixer Mixer;
    public GameObject deathUi;
    public GameObject Enemys;
    bool canPause = true;
    PlayerControler link;

    private void Start()
    {
        link = FindObjectOfType<PlayerControler>();
        StartCoroutine(FadeMixerGroup.StartFade(Mixer, "Volume", 3, 1));
        Z = FindObjectOfType<AudioManager>();
        Z.Play("Theme");
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        GameObject.Find("Transicion_Scene").GetComponent<Animator>().Play("Transicion");
    }
    void Update()
    {
        Bomb();
        llave();
        rupia();

        if (Input.GetKeyDown(KeyCode.Escape) && canPause)
        {
            _IsPaused = !_IsPaused;

            if (_IsPaused)
            {
                Z.PauseItemsManager();
                Z.Play("Pausa");
            }
            else
            {
                Z.PlayItemsManager();
                Z.Play("Pausa");
            }
        }
        PausaMenuFuntion();

        if (link.CurrentHealt>0)
        {
            canPause = true;
        }
        else
        {
            canPause = false;
        }
    }
    void PausaMenuFuntion()
    {
        if (canPause)
        {
            if (_IsPaused)
            {
                _PausedMenu.SetActive(true);
                Time.timeScale = 0;
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
            }
            else
            {
                _PausedMenu.SetActive(false);
                Time.timeScale = 1;
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
            }
        }
    }

    public void Pausa()
    {
        _IsPaused = !_IsPaused;
        Z.Play("Pausa");
        Z.PlayItemsManager();
    }

    void Bomb()
    {
        if (Bombs < 10)
        {
            NumBombs.text = "0" + Bombs.ToString();
        }
        else
        {
            NumBombs.text = Bombs.ToString();
        }
    }    
    void llave()
    {
        if (Llave < 10)
        {
            NumLlaves.text = "0" + Llave.ToString();
        }
        else
        {
            NumLlaves.text = Llave.ToString();
        }
    }   
    void rupia()
    {
        if (Rupias < 10)
        {
            NumRupias.text = "0" + Rupias.ToString();
        }
        else
        {
            NumRupias.text = Rupias.ToString();
        }
    }

    public void quit()
    {
        Application.Quit();
    }

    public void restar()
    {
        Z.Play("Pausa");
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
