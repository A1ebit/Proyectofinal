﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerControler : MonoBehaviour
{
    public GameObject bomb;
    public float speed = 25f;
    public Animator anim;
    public Rigidbody2D rb;
    public bool atke;
    public bool Invencible;
    Vector2 vb;
    public GameObject sword;
    public Transform spawnPosition;
    public float time;
    public float spawntime;
    Vector3 position;
    public float CurrentHealt;
    public float MaxHealt;
    public float CurrentHearts = 3;
    public float MaxHearts = 6;
    public Image[] Hearts;
    public Sprite full;
    public Sprite half;
    public Sprite empty;
    Manager ca;
    AudioManager Audio;
    void Start()
    {
        time = 0f;
        spawntime = 0.25f;
        CurrentHealt = CurrentHearts * 2;
        MaxHealt = CurrentHealt;
        ca = GameObject.Find("GameManager").GetComponent<Manager>();
        Audio = FindObjectOfType<AudioManager>();
    }
    void Update()
    {
        if (CurrentHearts > MaxHearts)
        {
            CurrentHearts = MaxHearts;
            MaxHealt = 12;
        }

        if (CurrentHealt > MaxHealt)
        {
            CurrentHealt = MaxHealt;
        }

        if (CurrentHealt > 3f)
        {
            Audio.Stop("Lowhealth");
        }

        InitHearts();
        UpdateHearts();
        Usarbomb();
        espadadirection();
        death();

        if (Input.GetKey(KeyCode.Space) && ca.espada==true)
        {
            atke = true;
            rb.velocity = new Vector2(0, 0);
            time += Time.deltaTime;

        }
        else if (Input.GetKeyUp(KeyCode.Space) && ca.espada==true)
        {
            rb.velocity = new Vector2(0, 0);
            if (time < spawntime)
            {
                anim.SetBool("atk", true);
                StartCoroutine("stop");
            }
            else
            {
                lanzarEspada();
            }
            time = 0f;
        }
        else if (atke == false && CurrentHealt>0)
        {
            movimiento();
        }

        Animan();
    }

    void Animan()
    {
        if (Input.GetKey(KeyCode.D))
        {
            anim.SetBool("Side", true);
        }
        else
        {
            anim.SetBool("Side", false);
        }

        if (Input.GetKey(KeyCode.W))
        {
            anim.SetBool("Up", true);
        }
        else
        {
            anim.SetBool("Up", false);
        }

        if (Input.GetKey(KeyCode.S))
        {
            anim.SetBool("Down", true);
        }
        else
        {
            anim.SetBool("Down", false);
        }

        if (Input.GetKey(KeyCode.A))
        {
            anim.SetBool("Side1", true);
        }
        else
        {
            anim.SetBool("Side1", false);
        }
    }

    void movimiento()
    {
        vb = new Vector2(0, 0);
        if (Input.GetKey(KeyCode.D))
        {
            //vb = new Vector2(Mathf.Abs(this.gameObject.transform.position.x+1), 0); 
            vb = Vector2.right;
        }
        else if (Input.GetKey(KeyCode.A))
        {
           //transform.Translate(-Vector2.right * speed * Time.deltaTime);
            vb = -Vector2.right;
        }
        else if (Input.GetKey(KeyCode.W))
        {
            //transform.Translate(Vector2.up * speed * Time.deltaTime);
            vb = Vector2.up;
        }
        else if (Input.GetKey(KeyCode.S))
        {
            //transform.Translate(-Vector2.up * speed * Time.deltaTime);
            vb = -Vector2.up;
        }
        rb.velocity = vb*speed;
    }

    void espadadirection()
    {
        if (Input.GetKey(KeyCode.D))
        {
            position = new Vector3(0, 0, -90);
        }
        else if (Input.GetKey(KeyCode.A))
        {
            position = new Vector3(0, 0, 90);
        }
        else if (Input.GetKey(KeyCode.W))
        {
            position = new Vector3();
        }
        else if (Input.GetKey(KeyCode.S))
        {
            position = new Vector3(0, 0, -180);
        }
    }

    IEnumerator stop()
    {
        yield return new WaitForSeconds(0.6f);
        anim.SetBool("atk", false);
        atke = false;
    }

    void lanzarEspada()
    {
        Audio.Play("SwordShoot");
        Instantiate(sword, spawnPosition.position, Quaternion.Euler(position));
        atke = false;
    }

    public void InitHearts()
    {
        for (int i = 0; i < MaxHearts; i++)
        {
            if (CurrentHearts <= i)
            {
                Hearts[i].enabled = false;
            }
            else
            {
                Hearts[i].enabled = true;
            }
        }
    }
    public void UpdateHearts()
    {
        float tempVida = CurrentHealt / 2;
        for (int i = 0; i < CurrentHearts; i++)
        {
            if (i <= tempVida - 1)
            {
                Hearts[i].sprite = full;
            }
            else if (i >= tempVida)
            {
                Hearts[i].sprite = empty;
            }
            else
            {
                Hearts[i].sprite = half;
            }
        }
    }

    public void damage()
    {
        Invencible = true;
        CurrentHealt -= 1;
        Audio.Play("LinkHurt");
        if (CurrentHealt <= 3f)
        {
            Audio.Play("Lowhealth");
        }
        StartCoroutine("Damagecolor");
    }

    void death()
    {
        if (CurrentHealt<=0)
        {
            FindObjectOfType<AudioManager>().PauseItemsManager();
            FindObjectOfType<AudioManager>().Play("LinkDie");//deathSounds
            rb.velocity = Vector2.zero;
            this.enabled = false;
            rb.velocity = Vector2.zero;
            anim.Play("Death");
            ca.deathUi.SetActive(true);
            ca.Enemys.SetActive(false);
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
    }
    IEnumerator Damagecolor()
    {
        SpriteRenderer ad = GetComponent<SpriteRenderer>();
        for (int i = 0; i < 8; i++)
        {
            ad.color = new Color(1, 0.5f, 0, 1);
            yield return new WaitForSeconds(0.2f);
            ad.color = new Color(0, 0.5f, 1, 1);
            yield return new WaitForSeconds(0.2f);
            ad.color = new Color(1, 1, 1, 1);
            yield return new WaitForSeconds(0.2f);
        }
        Invencible = false;
    }

    public void more()
    {
        CurrentHearts += 1;
        InitHearts();
        CurrentHealt += 2;
        MaxHealt += 2;
    }

    void Usarbomb()
    {
        Manager ca = GameObject.Find("GameManager").GetComponent<Manager>();
        if (Input.GetKeyDown(KeyCode.E) && ca.Bombs > 0)
        {
            Audio.Play("BombPoner");
            Instantiate(bomb, spawnPosition.position, spawnPosition.rotation);
            ca.Bombs--;
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (Invencible==false)
        {
            if (collision.tag == "Enemy")
            {
                damage();
            }
        }
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (Invencible == false)
        {
            if (collision.collider.tag == "Enemy")
            {
                damage();
            }
        }
    }
}
