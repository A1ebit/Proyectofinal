﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockrangeShoot : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player" || collision.tag == "NPC")
        {
            if (this.gameObject.tag=="RangeShoot")
            {
                this.gameObject.transform.parent.gameObject.GetComponent<EnemyController>().shooting=true;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player" || collision.tag == "NPC")
        {
            if (this.gameObject.tag == "RangeShoot")
            {
                this.gameObject.transform.parent.gameObject.GetComponent<EnemyController>().shooting = false;
                this.gameObject.transform.parent.gameObject.GetComponent<EnemyController>().time = 0.5f;
            }
        }
    }

}
