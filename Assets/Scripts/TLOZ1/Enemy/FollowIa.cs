﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowIa : MonoBehaviour
{
    public int Health=2;
    public float speed = 20f;
    public float timekncok = 0.3f;
    public float time;
    public float timeD;
    public float xmax;
    public float xmin;
    public bool Knock;
    public bool right;
    public bool left;
    public bool up;
    public bool down;
    public int x;
    public GameObject[] drops;
    public Rigidbody2D rb;

    void Start()
    {
        x = Random.Range(0, 4);
        rb = this.gameObject.GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        Physics2D.IgnoreCollision(GameObject.FindGameObjectWithTag("Player").GetComponent<Collider2D>(), GetComponent<Collider2D>());
    }
    // Update is called once per frame
    void Update()
    {
        timeD += Time.deltaTime;
        if (timeD>=1.8f)
        {
            Changedirection();
        }

        knockdir();

        if (Knock==true)
        {
            time += Time.deltaTime;
        }
        else
        {
            MoveNextPoint();
        }

        if (time>=timekncok)
        {
            time = 0;
            Knock = false;
        }

        Vector3 p = transform.position;
        if (p.x<xmin)
        {
            p.x = xmin;
            timeD=0f;
            x = 0;
        }
        else if (p.x > xmax)
        {
            p.x = xmax;
            timeD = 0f;
            x = 1;
        }

        if (Health<=0)
        {
            Drop();
            FindObjectOfType<AudioManager>().Play("EnemyDie");
            this.GetComponent<Animator>().Play("Death");
            rb.velocity = Vector3.zero;
        }

    }

    void MoveNextPoint()
    {
        switch (x)
        {
            case 0:
                rb.velocity = Vector3.right * speed;
                this.gameObject.GetComponent<Animator>().Play("Side");
                this.gameObject.transform.localScale = new Vector3(11.70852f, 11.70852f, 11.70852f);
                break;     
            case 1:
                rb.velocity = -Vector3.right * speed;
                this.gameObject.GetComponent<Animator>().Play("Side");
                this.gameObject.transform.localScale = new Vector3(-11.70852f, 11.70852f, 11.70852f);
                break;       
            case 3:
                rb.velocity = Vector3.up * speed;
                this.gameObject.GetComponent<Animator>().Play("down");
                this.gameObject.transform.localScale = new Vector3(11.70852f, 11.70852f, 11.70852f);
                break;     
            case 2:
                rb.velocity = -Vector3.up * speed;
                this.gameObject.GetComponent<Animator>().Play("idle");
                this.gameObject.transform.localScale = new Vector3(11.70852f, 11.70852f, 11.70852f);
                break;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {

        if (other.gameObject.name == "espada_collider" || other.gameObject.name == "EspadaUsar(Clone)")
        {
            StartCoroutine("Damagecolor");
            Knock = true;
            Rigidbody2D rb = this.gameObject.GetComponent<Rigidbody2D>();
            if (right == true)
            {
                rb.velocity = new Vector3(75, 0, 0);
            }
            else if (left == true)
            {
                rb.velocity = new Vector3(-75, 0, 0);
            }
            else if (up == true)
            {
                rb.velocity = new Vector3(0, 75, 0);
            }
            else if (down == true)
            {
                rb.velocity = new Vector3(0, -75, 0);
            }
            Health--;
            FindObjectOfType<AudioManager>().Play("EnemyHit");
        }
        else if (other.tag == "Bomb")
        {
            int y = Random.Range(0, 4);
            StartCoroutine("Damagecolor");
            Knock = true;
            switch (y)
            {
                case 0:
                    rb.velocity = new Vector3(75, 0, 0);
                    break;
                case 1:
                    rb.velocity = new Vector3(-75, 0, 0);
                    break;
                case 3:
                    rb.velocity = new Vector3(0, 75, 0);
                    break;
                case 2:
                    rb.velocity = new Vector3(0, -75, 0);
                    break;
            }
            Health--;
        }
        if (other.tag == "Enviroment")
        {
            timeD = 2.4f;
        }

    }

    void knockdir()
    {
        if (Input.GetKey(KeyCode.D))
        {
            right = true;
            down = false;
            left = false;
            up = false;
        }

        if (Input.GetKey(KeyCode.A))
        {
            left = true;
            right = false;
            down = false;
            up = false;
        }
    
        if (Input.GetKey(KeyCode.W))
        {
            up = true;
            right = false;
            left = false;
            down = false;
        }

        if (Input.GetKey(KeyCode.S))
        {
            down = true;
            right = false;
            left = false;
            up = false;
        }

    }

    void Changedirection()
    {
        timeD = 0;
        x = Random.Range(0, 4);
    }

    IEnumerator Damagecolor()
    {
        SpriteRenderer ad = GetComponent<SpriteRenderer>();
        for (int i = 0; i < 2; i++)
        {
            ad.color = new Color(1, 0.5f, 0, 1);
            yield return new WaitForSeconds(0.15f);
            ad.color = new Color(0, 0.5f, 1, 1);
            yield return new WaitForSeconds(0.15f);
            ad.color = new Color(1, 1, 1, 1);
            yield return new WaitForSeconds(0.15f);
        }
    }

    void Drop()
    {
        int t = Random.Range(0, 5);
        Transform SpawnPoint = this.gameObject.transform;
        Instantiate(drops[t], SpawnPoint.position, SpawnPoint.rotation);
    }
}
