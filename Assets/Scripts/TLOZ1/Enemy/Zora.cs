﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zora : MonoBehaviour
{
    public GameObject x;
    public float y=0f;
    void Update()
    {
        if (x.activeInHierarchy)
        {
            y += Time.deltaTime;
        }
        if (y>=4.789f)
        {
            x.SetActive(false);
            y = 0f;
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            x.SetActive(true);
            StartCoroutine(SoundStart());
        }
    }

    IEnumerator SoundStart()
    {
        yield return new WaitForSeconds(0.6f);
        FindObjectOfType<AudioManager>().Play("Zora");
    }
}
