﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class FollowIaNoKnocbac : MonoBehaviour
{
    public List<Transform> points;
    public int nextID;
    public int Health = 2;
    int IdChangeValue = 1;
    public float speed = 20f;


    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        MoveNextPoint();
    }

    private void Reset()
    {
        Init();
    }
    void Init()
    {
        GetComponent<Collider2D>().isTrigger = true;
        GameObject root = new GameObject(name + "_root");
        root.transform.position = transform.position;
        transform.SetParent(root.transform);
        GameObject Waypoints = new GameObject("Waypoints");
        Waypoints.transform.SetParent(root.transform);
        Waypoints.transform.position = root.transform.position;
        GameObject p1 = new GameObject("Waypoint1");
        p1.transform.SetParent(Waypoints.transform);
        p1.transform.position = root.transform.position;
        GameObject p2 = new GameObject("Waypoint2");
        p2.transform.SetParent(Waypoints.transform);
        p2.transform.position = root.transform.position;

        points = new List<Transform>();
        points.Add(p1.transform);
        points.Add(p2.transform);

    }

    void MoveNextPoint()
    {
        Transform goalpoint = points[nextID];
        if (goalpoint.transform.position.x > transform.position.x)
        {
            this.gameObject.GetComponent<Animator>().Play("Side");
        }
        if (goalpoint.transform.position.x < transform.position.x)
        {
            this.gameObject.GetComponent<Animator>().Play("Side");
            this.gameObject.transform.localScale = new Vector3(-11.70852f, 11.70852f, 11.70852f);
        }
        else
        {
            this.gameObject.transform.localScale = new Vector3(11.70852f, 11.70852f, 11.70852f);
        }

        if (goalpoint.transform.position.y > transform.position.y)
        {
            this.gameObject.GetComponent<Animator>().Play("down");
        }
        if (goalpoint.transform.position.y < transform.position.y)
        {
            this.gameObject.GetComponent<Animator>().Play("idle");
        }
        transform.position = Vector3.MoveTowards(transform.position, goalpoint.position, speed * Time.deltaTime);
        if (Vector2.Distance(transform.position, goalpoint.position) < 0.01f)
        {
            if (nextID == points.Count - 1)
            {
                IdChangeValue = -1;
            }
            if (nextID == 0)
            {
                IdChangeValue = 1;
            }
            nextID += IdChangeValue;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {

        if (other.gameObject.name == "espada_collider" || other.gameObject.name == "EspadaUsar(Clone)")
        {

            if (Health > 1)
            {
                Health--;
            }
            else
            {
                this.GetComponent<Animator>().Play("Death");
            }
        }

    }
}
