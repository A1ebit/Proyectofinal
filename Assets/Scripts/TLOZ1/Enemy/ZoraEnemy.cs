﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoraEnemy : MonoBehaviour
{
    public bool shooting = false;
    public GameObject Proyectile;
    public float time = 0.5f;
    void Update()
    {
        if (shooting == true)
        {
            time += Time.deltaTime;
            Shoot();
        }
    }

    public void Shoot()
    {
        if (time>=0.525f)
        {
            Transform SpawnPoint = this.gameObject.transform.Find("SpawnPoint");
            Instantiate(Proyectile, SpawnPoint.position, SpawnPoint.rotation);
            time = 0.5f;
            shooting = false;
        }
            
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.name == "espada_collider" || other.gameObject.name == "EspadaUsar(Clone)" || other.tag == "Bomb")
        {
            this.GetComponent<Animator>().Play("Death");
            FindObjectOfType<AudioManager>().Play("EnemyDie");
            Destroy(this.gameObject.transform.parent.gameObject, 0.5f);
        }

    }
}
