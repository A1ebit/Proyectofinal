﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class EnemyController : MonoBehaviour
{
    public List<Transform> points;
    public int nextID;
    int IdChangeValue = 1;
    public float speed = 20f;
    public float time = 0f;
    public bool shooting = false;
    public GameObject Proyectile;
    public GameObject[] drops;

    void Start()
    {
        time = 0.5f;
    }

    // Update is called once per frame
    void Update()
    {
        MoveNextPoint();
        /*if (Input.GetKeyDown(KeyCode.P))
        {
            Shoot();
        }*/

        if (shooting ==true)
        {
            time += Time.deltaTime;
            Shoot();
        }
    }

    private void Reset()
    {
        Init();
    }   
     void Init()
     {
        GetComponent<Collider2D>().isTrigger = true;
        GameObject root = new GameObject(name + "_root");
        root.transform.position = transform.position;
        transform.SetParent(root.transform);
        GameObject Waypoints = new GameObject("Waypoints");
        GameObject SpawnPoint = new GameObject("SpawnPoint");
        GameObject LockShoot = new GameObject("LockShoot");
        Waypoints.transform.SetParent(root.transform);
        SpawnPoint.transform.SetParent(this.gameObject.transform);
        LockShoot.transform.SetParent(this.gameObject.transform);
        Waypoints.transform.position= root.transform.position;
        SpawnPoint.transform.position = new Vector3(root.transform.position.x,root.transform.position.y + 4.7f,root.transform.position.z);
        LockShoot.transform.position = new Vector3(root.transform.position.x,root.transform.position.y + 39.2f,root.transform.position.z);
        LockShoot.transform.localScale = new Vector3(root.transform.localScale.x,root.transform.localScale.y + 5.3f,root.transform.localScale.z);
        BoxCollider2D sc = LockShoot.AddComponent(typeof(BoxCollider2D)) as BoxCollider2D;
        GameObject p1 = new GameObject("Waypoint1");
        p1.transform.SetParent(Waypoints.transform);
        p1.transform.position = root.transform.position;
        GameObject p2 = new GameObject("Waypoint2");
        p2.transform.SetParent(Waypoints.transform);
        p2.transform.position = root.transform.position;

        points = new List<Transform>();
        points.Add(p1.transform);
        points.Add(p2.transform);

     }

    void MoveNextPoint()
    {
        Transform goalpoint = points[nextID];
        if (goalpoint.transform.position.x>transform.position.x)
        {
            this.gameObject.transform.rotation = Quaternion.Euler(0,0,-90);
        }
        if (goalpoint.transform.position.x < transform.position.x)
        {
            this.gameObject.transform.rotation = Quaternion.Euler(0, 0, 90);
        }       
        if (goalpoint.transform.position.y>transform.position.y)
        {
            this.gameObject.transform.rotation = Quaternion.Euler(0,0,0);
        }
        if (goalpoint.transform.position.y < transform.position.y)
        {
            this.gameObject.transform.rotation = Quaternion.Euler(0, 0, -180);

        }
        transform.position = Vector3.MoveTowards(transform.position, goalpoint.position, speed * Time.deltaTime);
        if (Vector2.Distance(transform.position, goalpoint.position)<0.01f)
        {
            if (nextID==points.Count-1)
            {
                IdChangeValue = -1;
            }
            if (nextID==0)
            {
                IdChangeValue = 1;
            }
            nextID += IdChangeValue;
        }
    }

    public void Shoot()
    {
        if (time>=1)
        {
            Transform SpawnPoint = this.gameObject.transform.Find("SpawnPoint");
            Instantiate(Proyectile, SpawnPoint.position, SpawnPoint.rotation);
            time = 0;
        }

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.name == "espada_collider" || other.gameObject.name == "EspadaUsar(Clone)" || other.tag == "Bomb")
        {
            Drop();
            FindObjectOfType<AudioManager>().Play("EnemyDie");
            this.GetComponent<BoxCollider2D>().enabled = false;
            this.GetComponent<Animator>().Play("Death");
        }

    }
    void Drop()
    {
        int t = Random.Range(0, 5);
        Transform SpawnPoint = this.gameObject.transform;
        Instantiate(drops[t], SpawnPoint.position, (transform.rotation = Quaternion.Euler(0, 0, 0)));
    }
}
