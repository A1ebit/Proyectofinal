﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Changescene : MonoBehaviour
{
    [SerializeField]
    private Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag=="Player")
        {
            StartCoroutine(FadeMixerGroup.StartFade(FindObjectOfType<Manager>().Mixer, "Volume", 3, 0f));
            anim.Play("TRansicion_cambioScena");
            sceneControles.NextSceneNum(4);
            sceneControles.NextSceneControllesNum(2);
            StartCoroutine(end());
        }
    }

    IEnumerator end()
    {
        yield return new WaitForSeconds(3.5f);
        sceneControles.ChangeScene();
    }
}
