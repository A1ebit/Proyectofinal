﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NaviHey : MonoBehaviour
{
    public SoundCode[] sounds;

    void Awake()
    {
        foreach (SoundCode item in sounds)
        {
            item.Source = gameObject.AddComponent<AudioSource>();
            item.Source.clip = item.Sonido;
            item.Source.volume = item.volumen;
            item.Source.pitch = item.pitch;
            item.Source.loop = item.loop;
        }
    }

    public void Play()
    {
        StartCoroutine(Navi());
    }
    IEnumerator Navi()
    {
        sounds[0].Source.Play();
        yield return new WaitForSeconds(.5f);
        sounds[1].Source.Play();
    }
}
