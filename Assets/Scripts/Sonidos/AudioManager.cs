﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using System;


[Serializable]
public class AudiosSo
{
    public AudioSource Source;
    public bool pausa;
}
public class AudioManager : MonoBehaviour
{
    public SoundCode[] sounds;
    public AudiosSo[] Audios;

    // Start is called before the first frame update
    void Awake()
    {
        foreach (SoundCode item in sounds)
        {
            item.Source = gameObject.AddComponent<AudioSource>();
            item.Source.clip = item.Sonido;
            item.Source.volume = item.volumen;
            item.Source.pitch = item.pitch;
            item.Source.loop = item.loop;
            item.Source.playOnAwake = item.Awake;
            item.Source.outputAudioMixerGroup = item.Mixer;
        }
    }
    public void Play(string name)  //solo Uno
    {
        SoundCode item = Array.Find(sounds, sounds => sounds.name == name);
        if (item==null)
        {
            return;
        }
        if (item.Source.isPlaying==false)
        {
            item.Source.Play();
        }
        item.paused = false;
    }
    public void Pause(string name)   //solo Uno
    {
        SoundCode item = Array.Find(sounds, sounds => sounds.name == name);
        if (item==null)
        {
            return;
        }
        item.Source.Pause();
        item.paused = true;
    }   
    public void PauseItems()    //Items dentro de propioObjeto
    {
        foreach (var item in Audios)
        {
            if (item.Source.isPlaying)
            {
                item.Source.Pause();
                item.pausa = true;
            }
        }
    }
    public void PauseItemsManager()  //Items dentro del manager
    {
        foreach (var item in sounds)
        {
            if (item.Source.isPlaying)
            {
                item.Source.Pause();
                item.paused = true;
            }
        }
    }
    public void PlayItemsManager()    //Items dentro del manager
    {
        foreach (var item in sounds)
        {
            if (item.paused)
            {
                item.Source.UnPause();
                item.paused = false;
            }
        }
    }
    public void PlayItems()  //Items dentro de propioObjeto
    {
        foreach (var item in Audios)
        {
            if (item.pausa)
            {
                item.Source.UnPause();
                item.pausa = false;
            }
        }
    }
    public void Stop(string name)  //stop items, los reinicia
    {
        SoundCode item = Array.Find(sounds, sounds => sounds.name == name);
        if (item == null)
        {
            return;
        }
        item.Source.Stop();
    }
}
