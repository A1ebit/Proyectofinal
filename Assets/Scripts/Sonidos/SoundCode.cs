﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

[System.Serializable]
public class SoundCode
{
    public string name;
    public AudioClip Sonido;
    [Range(0,1)]
    public float volumen;
    [Range(0.1f, 3)]
    public float pitch;
    public bool loop;
    public bool Awake;
    [HideInInspector]
    public AudioSource Source;
    public bool paused;
    public AudioMixerGroup Mixer;
}
