﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Textos : MonoBehaviour
{
    public GameObject Square;
    public Text Texto;
    public Text Texto2;
    public bool rep=true;
    public bool error;
    [TextArea (0, 20)]
    public string sentence;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player") && rep == true)
        {
            FindObjectOfType<AudioManager>().Play("Pausa/Texto");
            Rigidbody2D A = other.GetComponent<Rigidbody2D>();
            Animator B = other.GetComponent<Animator>();
            other.GetComponent<PlayerControl>().enabled = false;
            B.SetBool("Movim",false);
            B.SetBool("Jump", false);      
            B.SetBool("aire",false);
            B.SetBool("down", false);
            A.velocity = Vector2.zero;
            Square.SetActive(true);
            rep = false;
            StartCoroutine(escribir());
            Debug.Log("Hello there");
        }  
    }

    IEnumerator escribir()
    {
        Texto.text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            Texto.text += letter;
            Texto2.text = Texto.text;
            FindObjectOfType<AudioManager>().Play("Text");
            yield return new WaitForSeconds(0.12f);
        }
        yield return new WaitForSeconds(1.45f);
        FindObjectOfType<AudioManager>().Play("Pausa/Texto");
        Square.SetActive(false);
        yield return new WaitForSeconds(0.1f);
        GameObject x = GameObject.FindGameObjectWithTag("Player");
        x.GetComponent<PlayerControl>().enabled = true;
        if (!error)
        {
            Destroy(this.gameObject);
        }
        else
        {
            yield return new WaitForSeconds(0.3f);
            rep = true;
        }
    }
}
