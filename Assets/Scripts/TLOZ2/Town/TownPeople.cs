﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TownPeople : MonoBehaviour
{
    public List<Transform> points;
    public int nextID;
    int IdChangeValue = 1;
    public float speed = 20f;
    public bool Mov;
    public bool Stop;

    void Start()
    {
        Mov = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (Mov)
        {
            MoveNextPoint();
            this.GetComponent<Animator>().SetBool("movimiento", true);
        }
        else
        {
            this.GetComponent<Animator>().SetBool("movimiento", false);
            GameObject Target = GameObject.FindGameObjectWithTag("Player");
            if (transform.position.x > Target.transform.position.x)
            {
                transform.localScale = new Vector3(3.4789f, 3.4789f, 3.4789f);
            }
            else
            {
                transform.localScale = new Vector3(-3.4789f, 3.4789f, 3.4789f);
            }
        }
    }

    private void Reset()
    {
        Init();
    }
    void Init()
    {
        GameObject root = new GameObject(name + "_root");
        root.transform.position = transform.position;
        transform.SetParent(root.transform);
        GameObject Waypoints = new GameObject("Waypoints");
        Waypoints.transform.SetParent(root.transform);
        Waypoints.transform.position = root.transform.position;
        GameObject p1 = new GameObject("Waypoint1");
        p1.transform.SetParent(Waypoints.transform);
        p1.transform.position = root.transform.position;
        GameObject p2 = new GameObject("Waypoint2");
        p2.transform.SetParent(Waypoints.transform);
        p2.transform.position = root.transform.position;

        points = new List<Transform>();
        points.Add(p1.transform);
        points.Add(p2.transform);

    }

    void MoveNextPoint()
    {
        Transform goalpoint = points[nextID];
        if (goalpoint.transform.position.x > transform.position.x)
        {
            this.gameObject.transform.localScale = new Vector3(-3.4789f, 3.4789f, 3.4789f);
        }
        if (goalpoint.transform.position.x < transform.position.x)
        {
            this.gameObject.transform.localScale = new Vector3(3.4789f, 3.4789f, 3.4789f);
        }

        transform.position = Vector3.MoveTowards(transform.position, goalpoint.position, speed * Time.deltaTime);
        if (Vector2.Distance(transform.position, goalpoint.position) < 0.01f)
        {
            if (nextID == points.Count - 1)
            {
                IdChangeValue = -1;
            }
            if (nextID == 0)
            {
                if (Stop && this.gameObject.name == "Townkid")
                {
                    Destroy(this.transform.parent.gameObject);
                }
                else if (Stop)
                {
                    Mov=false;
                }
                IdChangeValue = 1;
            }
            nextID += IdChangeValue;
        }
    }
}
