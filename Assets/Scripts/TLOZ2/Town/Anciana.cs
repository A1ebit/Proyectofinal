﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Anciana : MonoBehaviour
{
    public GameObject Target;
    public bool on;
    public GameObject Square;
    public Text Texto;
    public Text Texto2;
    [TextArea(0, 20)]
    public string sentence;

    // Update is called once per frame
    void Update()
    {
        Target = GameObject.FindGameObjectWithTag("Player");
        if (transform.position.x>Target.transform.position.x)
        {
            transform.localScale = new Vector3(3.4789f, 3.4789f, 3.4789f);
        }
        else
        {
            transform.localScale = new Vector3(-3.4789f, 3.4789f, 3.4789f);
        }

        if (on && Input.GetKeyDown(KeyCode.W))
        {
            FindObjectOfType<AudioManager>().Play("Pausa/Texto");
            Rigidbody2D A = GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody2D>();
            Animator B = GameObject.FindGameObjectWithTag("Player").GetComponent<Animator>();
            A.transform.gameObject.GetComponent<PlayerControl>().enabled = false;
            B.SetBool("Movim", false);
            B.SetBool("Jump", false);
            B.SetBool("aire", false);
            B.SetBool("down", false);
            A.velocity = Vector2.zero;
            Square.SetActive(true);
            StartCoroutine(escribir());
            on = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            on = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            on = false;
        }
    }
    IEnumerator escribir()
    {
        Texto.text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            Texto.text += letter;
            Texto2.text = Texto.text;
            FindObjectOfType<AudioManager>().Play("Text");
            yield return new WaitForSeconds(0.12f);
        }
        yield return new WaitForSeconds(1.45f);
        FindObjectOfType<AudioManager>().Play("Pausa/Texto");
        Square.SetActive(false);
        yield return new WaitForSeconds(0.1f);
        GameObject x = GameObject.FindGameObjectWithTag("Player");
        x.GetComponent<PlayerControl>().enabled = true;
        GetComponent<BoxCollider2D>().enabled = false;
        yield return new WaitForSeconds(1.2f);
        GetComponent<BoxCollider2D>().enabled = true;
    }
}
