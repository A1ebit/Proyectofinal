﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealLadyAfter : MonoBehaviour
{
    public bool on;
    public bool done;
    public bool heal;
    public GameObject Square;
    public Text Texto;
    public Text Texto2;
    [TextArea(0, 20)]
    public string sentence;
    private Manager2 manager2;

    private void Start()
    {
        manager2 = GameObject.FindObjectOfType<Manager2>();
    }
    void Update()
    {
        if (on && Input.GetKeyDown(KeyCode.W))
        {
            GameObject Pl = GameObject.FindGameObjectWithTag("Player");
            Pl.GetComponent<PlayerControl>().rb.velocity = Vector3.zero;
            Pl.GetComponent<PlayerControl>().enabled = false;
            Pl.GetComponent<SpriteRenderer>().enabled = false;
            FindObjectOfType<AudioManager>().Play("Pausa/Texto");
            Square.SetActive(true);
            StartCoroutine("escribir");
        }

        if (heal)
        {
            if (manager2.currenthealth!=manager2.Maxhealth)
            {
                FindObjectOfType<AudioManager>().Play("HealthRestore");
                manager2.currenthealth = Mathf.MoveTowards(manager2.currenthealth, manager2.Maxhealth, Time.deltaTime*2);
            }
            else
            {
                heal = false;
                done = true;
                FindObjectOfType<AudioManager>().Stop("HealthRestore");
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            on = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            on = false;
        }
    }

    IEnumerator escribir()
    {
        Texto.text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            Texto.text += letter;
            Texto2.text = Texto.text;
            FindObjectOfType<AudioManager>().Play("Text");
            yield return new WaitForSeconds(0.12f);
        }
        yield return new WaitForSeconds(1.45f);
        FindObjectOfType<AudioManager>().Play("Pausa/Texto");
        Square.SetActive(false);
        yield return new WaitForSeconds(0.1f);
        GameObject x = GameObject.FindGameObjectWithTag("Player");
        GetComponent<BoxCollider2D>().enabled = false;
        heal = true;
        //yield return new WaitForSeconds(1f);
        yield return new WaitUntil(() => done == true);
        done = false;
        yield return new WaitForSeconds(1f);
        x.GetComponent<PlayerControl>().enabled = true;
        x.GetComponent<SpriteRenderer>().enabled = true;
        yield return new WaitForSeconds(1f);
        GameObject.Find("Puta_root/Trigger").GetComponent<TextoPta>().end = true;
    }
}
