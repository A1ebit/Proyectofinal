﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextoPta : MonoBehaviour
{
    public GameObject Square;
    public GameObject Pt;
    public GameObject Puerta;
    public Text Texto;
    public Text Texto2;
    public bool rep;
    public bool start;
    public bool end;
    [TextArea(0, 20)]
    public string sentence;   
    public float speed;
    public Transform[] goalpoint;
    AudioManager audios;

    private void Start()
    {
        audios = FindObjectOfType<AudioManager>();
    }
    private void Update()
    {
        if (rep && Input.GetKeyDown(KeyCode.W))
        {
            audios.Play("Pausa/Texto");
            Rigidbody2D A = GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody2D>();
            Animator B = GameObject.FindGameObjectWithTag("Player").GetComponent<Animator>();
            A.transform.gameObject.GetComponent<PlayerControl>().enabled = false;
            B.SetBool("Movim", false);
            B.SetBool("Jump", false);
            B.SetBool("aire", false);
            B.SetBool("down", false);
            A.velocity = Vector2.zero;
            Square.SetActive(true);
            StartCoroutine(escribir());
            rep = false;
        }

        if (start && Pt!=null)
        {
            Pt.GetComponent<Animator>().SetBool("movimiento", true);
            Pt.transform.localScale=new Vector3(-1f, 1f, 1f);
            Pt.transform.position = Vector3.MoveTowards(Pt.transform.position, goalpoint[1].position, speed * Time.deltaTime);
            if (Vector2.Distance(Pt.transform.position, goalpoint[1].position) < 0.01f)
            {
                Pt.SetActive(false);
                start = false;
                Pt.GetComponent<Animator>().SetBool("movimiento", false);
            }
        }       

        if (end)
        {
            Pt.SetActive(true);
            Pt.transform.localScale = new Vector3(1f, 1f, 1f);
            Pt.GetComponent<Animator>().SetBool("movimiento", true);
            Pt.transform.position = Vector3.MoveTowards(Pt.transform.position, goalpoint[0].position, speed * Time.deltaTime);
            StartCoroutine("puerta");
            if (Vector2.Distance(Pt.transform.position, goalpoint[0].position) < 0.01f)
            {
                end = false;
                GetComponent<BoxCollider2D>().enabled = true;
                Pt.GetComponent<Animator>().SetBool("movimiento", false);
            }
        }

    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            rep = true;
        }
    }    
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            rep = false;
        }
    }

    IEnumerator escribir()
    {
        Texto.text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            Texto.text += letter;
            Texto2.text = Texto.text;
            audios.Play("Text");
            yield return new WaitForSeconds(0.12f);
        }
        yield return new WaitForSeconds(1.45f);
        Square.SetActive(false);
        audios.Play("Pausa/Texto");
        yield return new WaitForSeconds(0.1f);
        GameObject x = GameObject.FindGameObjectWithTag("Player");
        x.GetComponent<PlayerControl>().enabled = true;
        GetComponent<BoxCollider2D>().enabled = false;
        Puerta.SetActive(true);
        yield return new WaitForSeconds(1f);
        start = true;
    }  
    IEnumerator puerta()
    {
        yield return new WaitForSeconds(0.5f);
        Puerta.GetComponent<Animator>().Play("Puerta");
        yield return new WaitForSeconds(0.5f);
        Puerta.SetActive(false);
    }
}
