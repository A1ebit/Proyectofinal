﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shield : MonoBehaviour
{
    PlayerControl Link;
    private void Start()
    {
        Link = GetComponentInParent<PlayerControl>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "EnemyAtk")
        {
            FindObjectOfType<AudioManager>().Play("SwordHitShield");
            Link.Escudo = true;
        }
    }   
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "EnemyAtk")
        {
            Link.Escudo = false;
        }
    }
}
