﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    public float Speed;
    public static float nodmg;
    public static bool Invencible;
    public bool Invencibledaño;
    public static float InvencibleError;
    public float JumpForce;
    public Rigidbody2D rb;
    public float mov;
    public Animator anim;
    public bool isground;
    public bool damagere;
    public bool atk;
    public bool AireAtke;
    public bool AireAtkedown;
    public bool Escudo;
    private bool CorutineDamagerec = true;
    public Transform groundcheck;
    public float ratio;
    public LayerMask ground;
    private SpriteRenderer Link;
    AudioManager audios;
    private void Start()
    {
        Invencible = false;
        CorutineDamagerec = true;
        nodmg = 0;
        anim.SetBool("damage", false);
        audios = FindObjectOfType<AudioManager>();
        Link = GetComponent<SpriteRenderer>();
        anim = this.GetComponent<Animator>();
        rb = this.GetComponent<Rigidbody2D>();
    }
    void FixedUpdate()
    {
        isground = Physics2D.OverlapCircle(groundcheck.position, ratio, ground);
        mov = Input.GetAxisRaw("Horizontal");
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.L) && atk==false)
        {
            if (isground)
            {
                rb.velocity = Vector3.zero;
            }
            audios.Play("Sword");
            atk = true;
            anim.SetBool("atke", true);
            StartCoroutine(stop());
        }
        else if (atk == false)
        {
            if (damagere==true)
            {
                InvencibleError += Time.deltaTime;
                //rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y);
                if (isground && rb.velocity.y<=0.001f || InvencibleError>=5.5f)
                {
                    InvencibleError = 0;
                    damagere = false;
                    anim.SetBool("damage", false);
                }
            }
            else
            {
                movim();

            }
        }

        if (isground && AireAtke==true)
        {
            AireAtke = false;
            AireAtkedown = false;
            atk = false;
        }

        Invencibledaño = Invencible;
        airatk();
        Animations();
        RecDamage();
    }

    IEnumerator Jumping()
    {
        yield return new WaitForSeconds(0f);
        anim.SetBool("Jump", true);
        yield return new WaitForSeconds(0.05f);
        rb.velocity = new Vector2(rb.velocity.x, JumpForce);
        anim.SetBool("Jump", false);
    }

    void Animations()
    {
        if (mov != 0 && isground)
        {
            anim.SetBool("Movim", true);
        }
        else
        {
            anim.SetBool("Movim", false);
        }

        if (Input.GetKey(KeyCode.S) && isground)
        {
            anim.SetBool("down", true);
        }
        else
        {
            anim.SetBool("down", false);
        }

        if (isground == false)
        {
            anim.SetBool("aire", true);
        }
        else
        {
            anim.SetBool("aire", false);
        }
    }

    void movim()
    {
        if (Input.GetKeyDown(KeyCode.Space) && isground)
        {
            StartCoroutine("Jumping");
        }
        else if (Input.GetKey(KeyCode.S) && isground && rb.velocity.y <= 0.001f)
        {
                rb.velocity = Vector3.zero;
        }
        else
        {
            rb.velocity = new Vector2(mov * Speed, rb.velocity.y);
        }

        if (Input.GetKey(KeyCode.A))
        {
            this.transform.localScale = new Vector3(-4f, 3.5f, 4f);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            this.transform.localScale = new Vector3(4f, 3.5f, 4f);
        }

    }

    void RecDamage()
    {
        if (nodmg > 0)
        {
            Invencible = true;
            if (CorutineDamagerec)
            {
                CorutineDamagerec = false;
                StartCoroutine(stocolordmgp());
            }
            if (anim.GetBool("damage") == false)
            {
                nodmg -= Time.deltaTime;
            }
        }
        else
        {
            Invencible = false;
            Invencibledaño = Invencible;
            nodmg = 0;
            Link.color = new Color(1f, 1f, 1f, 1f);
        }
    }

    IEnumerator stop()
    {
        yield return new WaitForSeconds(0.35f);
        anim.SetBool("atke", false);
        atk = false;
    }   
    
    IEnumerator stocolordmgp()
    {
        while (Invencible)
        {
            yield return new WaitForSeconds(0.2f);
            Link.color = new Color(1f, 1f, 1f, 0f);
            yield return new WaitForSeconds(0.2f);
            Link.color = new Color(1f, 1f, 1f, 1f);
        }
        CorutineDamagerec = true;
    }

    void airatk()
    {
        if (isground==false && Input.GetKey(KeyCode.S) && damagere==false)
        {
            AireAtke = true;
            AireAtkedown = true;
            anim.Play("Atkdownair");
            atk = true;
            movim();
        }
        else if (isground == false && Input.GetKey(KeyCode.W) && damagere == false)
        {
            AireAtke = true;
            anim.Play("atkupair");
            atk = true;
            movim();
        }
        else if (isground == false && (Input.GetKeyUp(KeyCode.S)|| Input.GetKeyUp(KeyCode.W)) && damagere == false)
        {
            AireAtke = false;
            AireAtkedown = false;
            atk = false;
        }
    }

    public void DarkDeath()
    {
        Link.color = Color.black;
        this.enabled = false;
    }
}
