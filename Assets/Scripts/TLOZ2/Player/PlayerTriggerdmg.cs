﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTriggerdmg : MonoBehaviour
{
    private PlayerControl Link;
    private bool inv;
    private Vector3 X;
    void Start()
    {
        Link = this.transform.parent.GetComponent<PlayerControl>();
        Link.rb.AddForce(new Vector2(-8f, 12f), ForceMode2D.Impulse);
        FindObjectOfType<Manager2>();
    }
    private void Update()
    {
        inv = Link.Invencibledaño;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        X = collision.transform.position;
        if (collision.gameObject.tag == "Enemy" || collision.tag=="Proyectile" || collision.tag == "EnemyAtk")
        {
            if (inv == false)
            {
                //Debug.Log("iBeliveiCanFly");
                StartCoroutine(DañoDuele());
            }
        }
    }

    IEnumerator DañoDuele()
    {
        yield return new WaitForSeconds(0.01f);
        if (Link.Escudo==false)
        {
            Link.rb.velocity = Vector2.zero;
            Link.damagere = true;
            Link.anim.Play("hurt");
            Link.anim.SetBool("damage", true);
            if (Link.transform.position.x > X.x)
            {
                Link.rb.velocity = Vector3.zero;
                Link.rb.AddForce(new Vector2(8f, 12f), ForceMode2D.Impulse);
            }
            else
            {
                Link.rb.velocity = Vector3.zero;
                Link.rb.AddForce(new Vector2(-8f, 12f), ForceMode2D.Impulse);
            }
        }
    }
}

