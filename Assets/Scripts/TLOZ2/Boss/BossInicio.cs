﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossInicio : MonoBehaviour
{
    public SpriteRenderer Fondo;
    private GameObject Link;
    [SerializeField]
    private GameObject Anciano;
    [SerializeField]
    private GameObject _DarkLink;    
    [SerializeField]
    private Transform PadreInstate; 
    [SerializeField]
    private GameObject DarkLinkHealtbar;    
    [SerializeField]
    private Animator Fade; 
    private GameObject Pared;
    private PlayerControl LinkOG;
    public int changescene;
    public bool ks;
    Camera camer;
    [SerializeField]
    private GameObject triforce;
    bool End;
    AudioManager Audios;

    void Start()
    {
        Audios = FindObjectOfType<AudioManager>();
        End = true;
        ks = true;
        Pared = transform.GetChild(0).gameObject;
        Link = GameObject.FindGameObjectWithTag("Player");
        camer = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        LinkOG = Link.GetComponent<PlayerControl>();
    }

    void Update()
    {
        if (ks==true && DarkLink.health==0 && GameObject.FindObjectOfType<DarkLink>()!=null && LinkOG.isground)
        {
            if (End)
            {
                Audios.Play("DarkLinkInicio");
                Audios.Stop("DarkLinkBt");
                End = false;
                StartCoroutine(EndGame());
            }
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            ks = true;
            camer.position = changescene;
            Animator B = collision.GetComponent<Animator>();
            LinkOG.enabled = false;
            LinkOG.rb.velocity = Vector3.zero;
            B.SetBool("Movim", false);
            B.SetBool("Jump", false);
            B.SetBool("aire", false);
            B.SetBool("down", false);
            StartCoroutine(inicio());
            Destroy(this.GetComponent<Collider2D>());
        }

    }
    IEnumerator inicio()
    {
        yield return new WaitForSeconds(0.4f);
        Anciano.GetComponent<Animator>().Play("action");
        yield return new WaitForSeconds(1.4f);
        Anciano.GetComponent<Animator>().Play("static");
        yield return new WaitForSeconds(0.6f);
        Destroy(Anciano.transform.GetChild(0).gameObject);
        Anciano.SetActive(false);
        Pared.SetActive(true);
        Audios.Stop("Dungeon");
        Audios.Play("DarkLinkInicio");
        yield return new WaitForSeconds(0.2f);
        for (int i = 0; i < 6; i++)
        {
            yield return new WaitForSeconds(0.07f);
            Link.GetComponent<SpriteRenderer>().color = Color.white;
            Fondo.color = Color.white;
            Fondo.enabled = true;
            yield return new WaitForSeconds(0.07f);
            Link.GetComponent<SpriteRenderer>().color = Color.cyan;
            Fondo.color = Color.red;
            yield return new WaitForSeconds(0.07f);
            Link.GetComponent<SpriteRenderer>().color = Color.red;
            Fondo.color = Color.blue;
            yield return new WaitForSeconds(0.18f);
            Fondo.enabled = false;
        }
        StartCoroutine(DarkLinkApear());
        Audios.Play("DarkLinkInicio");
        Link.GetComponent<SpriteRenderer>().color = Color.white;
        Fondo.color = Color.white;
        Fondo.enabled = true;
        yield return new WaitForSeconds(0.5f);
        DarkLink Xd = GameObject.FindObjectOfType<DarkLink>();
        yield return new WaitUntil(()=> Xd.inicio == false);
        Xd.inicio = true;
        yield return new WaitForSeconds(0.8f);
        //Debug.Log("ya esta carnal");
        Link.GetComponent<PlayerControl>().enabled = true;
        Xd.inicio = false;
        Xd.gameObject.GetComponent<Animator>().SetBool("down", false);
        Audios.Play("DarkLinkBt");
    }
    IEnumerator DarkLinkApear()
    {
        yield return new WaitForSeconds(0.3f);
        Instantiate(_DarkLink, new Vector3(Link.transform.position.x, Link.transform.position.y+0.5f, Link.transform.position.z + 1.4f), Link.transform.rotation,PadreInstate);
        yield return new WaitForSeconds(0.1f);
        DarkLinkHealtbar.SetActive(true);
    } 
    IEnumerator EndGame()
    {
        yield return new WaitForSeconds(3.5f);
        ks = false;
        LinkOG.rb.velocity = Vector3.zero;
        LinkOG.enabled = false;
        yield return new WaitForSeconds(0.1f);
        Destroy(Fondo);
        Anciano.SetActive(true);
        Audios.Play("TriforceGet");
        Anciano.GetComponent<Animator>().Play("action");
        yield return new WaitForSeconds(0.25f);
        LinkOG.anim.Play("End");
        Instantiate(triforce, new Vector3(Link.transform.position.x, Link.transform.position.y + 2f, Link.transform.position.z), Link.transform.rotation, Link.transform);
        yield return new WaitForSeconds(3.5f);
        sceneControles.NextSceneNum(4);
        sceneControles.NextSceneControllesNum(3);
        Fade.Play("FadeOut");
        StartCoroutine(FadeMixerGroup.StartFade(FindObjectOfType<Manager2>().Mixer, "Volume", 3, 0));
        yield return new WaitForSeconds(3.5f);
        sceneControles.ChangeScene();
    }
}
