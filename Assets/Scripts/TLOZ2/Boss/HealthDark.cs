﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthDark : MonoBehaviour
{
    public RawImage[] corazones;
    void Start()
    {
        corazones = new RawImage[transform.childCount];
        for (int i = 0; i < transform.childCount; i++)
        {
            corazones[i] = transform.GetChild(i).gameObject.GetComponent<RawImage>();
        }
        System.Array.Reverse(corazones);
    }

    // Update is called once per frame
    void Update()
    {
        int vida = DarkLink.health;
        for (int i = 0; i < corazones.Length; i++)
        {
            if (i<=vida-1)
            {
                corazones[i].color = Color.red;
            }
            else
            {
                corazones[i].color = Color.white;
            }    
        }

        if (DarkLink.health==0)
        {
            Destroy(this.gameObject, 2f);
        }
    }
}
