﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DarkSword : MonoBehaviour
{
    // Start is called before the first frame update
    Manager2 manager2;
    private DarkLink Otosan;
    private PlayerControl Link;
    void Start()
    {
        Otosan = transform.parent.GetComponent<DarkLink>();
        manager2 = FindObjectOfType<Manager2>();
        Link = FindObjectOfType<PlayerControl>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "PlayerDMG")
        {
            if (PlayerControl.Invencible == false)
            {
                StartCoroutine(DañoLastimar());
            }
        }
    }

    IEnumerator DañoLastimar()
    {
        yield return new WaitForSeconds(0.01f);
        if (Link.Escudo == false)
        {
            FindObjectOfType<AudioManager>().Play("LinkHurt");
            manager2.TakeDamage(Otosan.damage);
        }
    }
}
