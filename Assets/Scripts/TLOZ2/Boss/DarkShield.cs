﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DarkShield : MonoBehaviour
{
    DarkLink Boss;
    private void Start()
    {
        Boss = GetComponentInParent<DarkLink>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.name == "sword")
        {
            FindObjectOfType<AudioManager>().Play("SwordHitShieldD");
            Boss.Escudo = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.name == "sword")
        {
            Boss.Escudo = false;
        }
    }
}
