﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DarkLink : MonoBehaviour
{
    // PerssonajeBoss
    public float Speed;
    public Rigidbody2D rb;
    public float damage;
    public Animator anim;
    Collider2D Support;
    [SerializeField]
    private bool isground;
    float ratio=0.05f;
    public LayerMask ground;
    [SerializeField]
    private Transform groundcheck;   
    [SerializeField]
    private Collider2D trigger;  
    public static int health;
    public float hp;
    private bool mov;
    [SerializeField]
    private bool atk;   
    [SerializeField]
    private float jump;
    private float direction;
    private SpriteRenderer dLink;

    //Variables codigo
    private int TipoAtk;
    public bool dolor;
   [SerializeField]
    private float StopDistance;
    GameObject Link;
    public bool inicio = true;
    public bool invencible;
    public bool saltocaminar;
    public bool saltocaminarnoatk;
    public float jumpPos;
    public float jumpPostime;
    public float distanciaX;
    public float distanciaY;
    public float nodmg;
    public bool Escudo;
    public int RandomN;

    AudioManager audios;

    void Start()
    {
        dLink = GetComponent<SpriteRenderer>();
        audios = FindObjectOfType<AudioManager>();
        jumpPostime = Random.Range(2f, 5f);
        RandomN = Random.Range(0, 4);
        StopDistance = 3.3f;
        Speed = 6.4f;
        jump = 20f;
        damage = 1f;
       // this.tag = "Untagged";
        Support = GetComponent<Collider2D>();
        health = 8;
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        Link = FindObjectOfType<PlayerControl>().gameObject;
        StartFight();
    }

    private void FixedUpdate()
    {
        isground = Physics2D.OverlapCircle(groundcheck.position, ratio, ground);
        Physics2D.IgnoreCollision(GameObject.FindGameObjectWithTag("Player").GetComponent<Collider2D>(),GetComponent<Collider2D>());
    }

    void Update()
    {
        hp = health;
        animations();
        Deathadios();
        Look();
        RecDamage();

        distanciaX = Mathf.Abs(transform.position.x - Link.transform.position.x);
        distanciaY = Mathf.Abs(transform.position.y - Link.transform.position.y);

        if (distanciaX < 0.4f && atk == false && inicio == false && isground)
        {
            StartCoroutine(JumpDk());
        }
        else if (distanciaX < StopDistance && atk==false && inicio == false)
        {
            if (isground)
            {
                if (distanciaY<0.03f)
                {
                    rb.velocity = Vector3.zero;
                    mov = false;
                    if (RandomN >0)
                    {
                        StartCoroutine(Atacar());
                        RandomN = Random.Range(0, 4);
                    }
                    else
                    {
                        RandomN = Random.Range(0, 2);
                        if (RandomN==0)
                        {
                            direction *= (-1);
                        }
                        StartCoroutine(JumpDk());
                        RandomN = Random.Range(0, 4);
                    }
                }
                else
                {
                    if (saltocaminarnoatk==false)
                    {
                        mov = false;
                        direction *= (-1);
                        StartCoroutine(JumpDk());
                        StartCoroutine(Atacar());
                    }
                }
            }
            else
            {
                if (saltocaminarnoatk == false)
                {
                    StartCoroutine(Atacar());
                }
            }
        }
        else if(atk==false)
        {
            if (dolor==false)
            {
                movim();
            }
        }

    }

    void animations()
    {
        if (isground)
        {
            if (inicio==true)
            {
                trigger.enabled = true;
                rb.velocity = Vector3.zero;
                anim.SetBool("down", true);
                anim.Play("Down");
                inicio = false;
            }
            else
            {
                anim.SetBool("aire", false);
            }
        }
        else if (isground==false && inicio==false)
        {
            anim.SetBool("aire", true);
        }

        if (mov && isground)
        {
            anim.SetBool("move", true);
        }
        else
        {
            anim.SetBool("move", false);
        }

        if (isground  && rb.velocity.y<=0.01f)
        {
            anim.SetBool("damage", false);
            dolor = false;
        }
    }

    void movim()
    {
        if (saltocaminar)
        {
            jumpPostime = Random.Range(0f, 5f);
            saltocaminar = false;
        }

        jumpPos += Time.deltaTime;
        if (jumpPos >= jumpPostime && inicio == false && isground)
        {
            jumpPos = 0F;
            jumpPostime = Random.Range(0, 4);
            rb.velocity = Vector3.zero;
            StartCoroutine(JumpDk());
            StartCoroutine(JumpDkNoatk());
            saltocaminar = true;
        }
        else if (isground && anim.GetBool("down") == true && rb.velocity.y < 0.01f)
        {
            rb.velocity = Vector3.zero;
        }
        else if (isground && inicio == false)
        {
            if (transform.position.x > Link.transform.position.x)
            {
                direction = -1;
            }
            else
            {
                direction = 1;
            }
            mov = true;
            rb.velocity = new Vector2(direction*Speed,rb.velocity.y);
            saltocaminarnoatk = false;
        }

    }

    void StartFight()
    {
        rb.velocity = Vector3.zero;
        rb.AddForce(new Vector2(8f, 20f), ForceMode2D.Impulse);
        anim.Play("Enter_dark");
    }

    void Look()
    {

        if (transform.position.x > Link.transform.position.x)
        {
            transform.localScale = new Vector3(4f, 3.5f, 4f);    // comienza aqui
        }
        else
        {
            transform.localScale = new Vector3(-4f, 3.5f, 4f);    
        }
    }

    void Deathadios()
    {
        if (health == 0)
        {
            Support.enabled = false;
            Destroy(this.gameObject, 2f);
        }
    }

    void RecDamage()
    {
        if (nodmg > 0)
        {
            invencible = true;
            if (anim.GetBool("damage") == false)
            {
                nodmg -= Time.deltaTime;
            }
        }
        else
        {
            invencible = false;
            nodmg = 0;
            dLink.color = new Color(1f, 1f, 1f, 1f);
        }
    }

    IEnumerator JumpDk()
    {
        anim.SetBool("salto", true);
        yield return new WaitForSeconds(0.05f);
        rb.velocity = new Vector2(direction * Speed, jump);
        anim.SetBool("salto", false);
    }   
    IEnumerator JumpDkNoatk()
    {
        yield return new WaitForSeconds(0.1f);
        saltocaminarnoatk = true;

    }

    IEnumerator Atacar()
    {       
        atk = true;
        TipoAtk = Random.Range(0, 3);
        if (TipoAtk==1)
        {
            anim.SetBool("down", true);

        }
        yield return new WaitForSeconds(0.14f);
        if (rb.velocity.y<0.01f)
        {
            rb.velocity = Vector2.zero;
        }
        anim.SetBool("atk", true);
        audios.Play("SwordD");
        yield return new WaitForSeconds(0.48f);
        anim.SetBool("atk", false);
        if (anim.GetBool("down"))
        {
            anim.SetBool("down", false);
        }
        yield return new WaitForSeconds(0.14f);
        atk = false;
    }
}
