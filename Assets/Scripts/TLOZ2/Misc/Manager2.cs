﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Manager2 : MonoBehaviour
{
    public Text Exp;
    private Text Exp2;
    public static int Explv;
    [SerializeField]
    private Slider healthFill;
    public float currenthealth;
    public float Maxhealth;  
    [SerializeField]
    private Slider MagicFill;  
    [SerializeField]
    private GameObject RedScer;
    private float currentmagic;
    private float Maxmagic;
    public AudioMixer Mixer;
    AudioManager Audio;
    [SerializeField]
    private GameObject Pausa;  
    [SerializeField]
    private GameObject Restart;
    public bool _IsPaused;
    bool _CanPaused;

    // Start is called before the first frame update
    void Start()
    {
        _CanPaused = true;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        _IsPaused = false;
        Audio = FindObjectOfType<AudioManager>();
        Explv = 0;
        Maxhealth = 7;
        Maxmagic = MagicFill.maxValue;
        currentmagic = Maxmagic;
        currenthealth = Maxhealth-1.5f;
        healthFill.value = currenthealth;
        Exp2 = Exp.transform.parent.GetComponent<Text>();
        Audio.Play("Village");
        StartCoroutine(FadeMixerGroup.StartFade(Mixer, "Volume", 3, 1));
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && _CanPaused)
        {
            _IsPaused = !_IsPaused;

            if (_IsPaused)
            {
                Audio.PauseItemsManager();
                Audio.Play("Pausa/Texto");
            }
            else
            {
                Audio.PlayItemsManager();
                Audio.Play("Pausa/Texto");
            }
        }
        EscPausa();
        healthFill.value = currenthealth;

        ExpGain();
        
        /*if (Input.GetKeyDown(KeyCode.V))
        {
            currentmagic-=1;
            MagicFill.value = currentmagic;
        }*/
        if (currenthealth > 3)
        {
            Audio.Stop("LinkLowHP");
        }
    }

    void ExpGain()
    {
        if (Explv < 10)
        {
            Exp.text = "000" + Explv.ToString();
        }
        else if (Explv<100)
        {
            Exp.text = "00" + Explv.ToString();
        }
        else if (Explv<1000)
        {
            Exp.text = "0" + Explv.ToString();
        }    
        else
        {
            Exp.text = Explv.ToString();
        }
        Exp2.text = Exp.text;
    }

    public void TakeDamage(float damage)
    {
        PlayerControl.nodmg = 2.2f;
        currenthealth -= damage;
        healthFill.value = currenthealth;
        if (currenthealth <= 3 && currenthealth>0)
        {
            Audio.Play("LinkLowHP");
        }
        if (currenthealth<=0f)
        {
            _CanPaused = false;
            Audio.PauseItemsManager();
            Audio.Play("LinkDie");
            RedScer.SetActive(true);
            PlayerControl D = FindObjectOfType<PlayerControl>();
            D.DarkDeath();
            D.enabled = false;
            StartCoroutine(DeathMoc(D));
            D.rb.velocity = Vector3.zero;
        }
    }  
    public static void ExpEnemy(int exp)
    {
        Explv = Explv + exp;
    }

    void EscPausa()
    {
        if (_CanPaused)
        {
            if (_IsPaused)
            {
                Pausa.SetActive(true);
                Time.timeScale = 0;
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
            }
            else
            {
                Pausa.SetActive(false);
                Time.timeScale = 1;
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
            }
        }

    }

    public void reanudar()
    {
        _IsPaused = !_IsPaused;
        Audio.PlayItemsManager();
        Audio.Play("Pausa/Texto");
    }
    public void quit()
    {
        Application.Quit();
    }

    public void restar()
    {
        Audio.Play("Pausa/Texto");
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    IEnumerator DeathMoc(PlayerControl D)
    {
        yield return new WaitForSeconds(.5f);
        Destroy(FindObjectOfType<PlayerTriggerdmg>().gameObject);
        yield return new WaitForSeconds(.01f);
        D.rb.velocity = Vector3.zero;
        yield return new WaitForSeconds(3.5f);
        Time.timeScale = 0;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        Restart.SetActive(true);
    }
}
