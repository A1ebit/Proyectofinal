﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextExp : MonoBehaviour
{
    public TextMesh a;
    public TextMesh b;
    private bool start;
    [HideInInspector]
    public int expSave;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(inicio());
    }

    private void Update()
    {
        if (start)
        {
            this.transform.position = Vector3.MoveTowards(this.transform.position, new Vector3 (this.transform.position.x,this.transform.position.y+5,this.transform.position.z) , Time.deltaTime*2);
        }
    }
    public void ExpInstance(int exp)
    {
        a.text = exp.ToString();
        b.text = a.text;
        expSave = exp;
    }
    IEnumerator inicio()
    {
        yield return new WaitForSeconds(0.17f);
        start = true;
        yield return new WaitForSeconds(1.6f);
        Manager2.ExpEnemy(expSave);
        FindObjectOfType<AudioManager>().Play("EXP");
        yield return new WaitForSeconds(0.05f);
        Destroy(this.gameObject);
    }
}
