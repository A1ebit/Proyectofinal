﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoneActive : MonoBehaviour
{
    public GameObject zone;
    public bool pez;
    public float time;
    [HideInInspector]
    public Transform father;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag=="Player")
        {
           zone.SetActive(true);
        }
    }   
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (pez)
            {
                for (int i = 0; i < father.childCount; i++)
                {
                    Destroy(father.GetChild(i).gameObject);
                }
            }
            zone.SetActive(false);
        }
    }

    private void Update()
    {
        if (pez && zone.activeInHierarchy)
        {
            time += Time.deltaTime;
            if (time >= 2.2f)
            {
                time = 0;
                GameObject.FindObjectOfType<Spawner>().invokar();
            }
        }
    }
}
