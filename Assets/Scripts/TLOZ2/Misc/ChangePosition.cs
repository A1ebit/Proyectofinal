﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangePosition : MonoBehaviour
{
    //public GameObject Link;
    private GameObject PlayerOg;
    private GameObject npcs;
    private GameObject HUD;
    private GameObject ambient;
    private GameObject Exp;
    private Transform Spawn;
    public int changescene;
    [SerializeField]
    private bool doit;
    public bool edif;
    public bool dungeon;
    public bool town;
    AudioManager audios;

    void Start()
    {
        audios = FindObjectOfType<AudioManager>();
        npcs = GameObject.Find("Characters");
        Exp = GameObject.Find("Exp_Mesagge");
        ambient = GameObject.Find("Grid");
        HUD = GameObject.Find("HUD");
        Spawn = this.transform.GetChild(0);
        PlayerOg = FindObjectOfType<PlayerControl>().gameObject;
    }

    void Update()
    {
        if (doit && Input.GetKeyDown(KeyCode.W))
        {
            StartCoroutine(change());
            GameObject camer = GameObject.FindGameObjectWithTag("MainCamera");
            Camera caman = camer.GetComponent<Camera>();
            caman.position = changescene;
            /*PlayerOg.SetActive(false);
            Instantiate(Link, Spawn.position, Spawn.rotation).transform.SetParent(npcs.transform);*/
            PlayerOg.transform.position = Spawn.position;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player" && dungeon)
        {
            StartCoroutine(change());
            GameObject camer = GameObject.FindGameObjectWithTag("MainCamera");
            Camera caman = camer.GetComponent<Camera>();
            caman.position = changescene;
            PlayerOg.transform.position=Spawn.position;
            audios.Play("Dungeon");
            audios.Stop("Village");
        }

        if (collision.tag == "Player" && edif)
        {
            doit = true;
        }

        else if (collision.tag == "Player" && town)
        {
            StartCoroutine(change());
            GameObject camer = GameObject.FindGameObjectWithTag("MainCamera");
            Camera caman = camer.GetComponent<Camera>();
            caman.position = changescene;
            PlayerOg.transform.position = Spawn.position;
            audios.Stop("Dungeon");
            audios.Play("Village");
        }
    }   
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player" && edif)
        {
            doit = false;
        }
    }

    IEnumerator change()
    {
        GameObject camer = GameObject.FindGameObjectWithTag("MainCamera");
        Camera caman = camer.GetComponent<Camera>();
        npcs.SetActive(false);
        HUD.SetActive(false);
        ambient.SetActive(false);
        Exp.SetActive(false);
        caman.enabled = false;
        yield return new WaitForSeconds(1f);
        caman.enabled = true;
        npcs.SetActive(true);
        ambient.SetActive(true);
        HUD.SetActive(true);
        Exp.SetActive(true);
    }
}
