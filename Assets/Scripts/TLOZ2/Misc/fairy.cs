﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class fairy : MonoBehaviour
{
    public Text lifelvl;
    private Text lifelvl2;
    [SerializeField]
    private GameObject borde;
    [SerializeField]
    private PlayerControl Link;   
    [SerializeField]
    private Animator Linkanim;
    [SerializeField]
    private SpriteRenderer linkImg;
    private Manager2 manager2;
    private bool heal;
    void Start()
    {
        lifelvl2 = lifelvl.transform.GetChild(0).GetComponent<Text>();
        Link = GameObject.FindObjectOfType<PlayerControl>();
        manager2 = GameObject.FindObjectOfType<Manager2>();
        linkImg = Link.transform.gameObject.GetComponent<SpriteRenderer>();
        Linkanim = Link.transform.gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (heal)
        {
            if (manager2.currenthealth != manager2.Maxhealth)
            {
                Link.rb.bodyType = RigidbodyType2D.Kinematic;
                Link.rb.velocity = Vector3.zero;
                Link.enabled = false;
                manager2.currenthealth = Mathf.MoveTowards(manager2.currenthealth, manager2.Maxhealth, Time.deltaTime * 2);
                FindObjectOfType<AudioManager>().Play("HealthRestore");
            }
            else
            {
                Link.rb.bodyType = RigidbodyType2D.Dynamic;
                Link.enabled = true;
                heal = false;
                FindObjectOfType<AudioManager>().Stop("HealthRestore");
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            FindObjectOfType<AudioManager>().Play("Fairy");
            FindObjectOfType<AudioManager>().Play("EXP");
            lifelvl.text = "8";
            lifelvl2.text = lifelvl.text;
            borde.SetActive(true);
            manager2.Maxhealth = 8f;
            heal = true;
            Linkanim.SetBool("Movim", false);
            StartCoroutine(Healing());
        }
    }

    IEnumerator Healing()
    {
        while (heal)
        {            
            yield return new WaitForSeconds(0.2f);
            linkImg.color = Color.blue;
            yield return new WaitForSeconds(0.2f);
            linkImg.color = new Color(1f, 1f, 1f, 1f);     
        }
        yield return new WaitUntil(()=> heal == false);
        Debug.Log("Hello");
        yield return new WaitForSeconds(0.3f);
        Destroy(this.gameObject);
    }
}
