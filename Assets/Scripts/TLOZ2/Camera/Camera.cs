﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class limit
{
    public string name;
    public float minval;
    public float maxval;
    public float ypos;
}
public class Camera : MonoBehaviour
{
    public Transform player;
    public limit[] limitpositions;
    public Vector3 limits;
    public int position;
    public RawImage HD;
    Color x;

    private void Start()
    {
        x = HD.color;
    }
    void Update()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        limits = new Vector3(Mathf.Clamp(player.position.x, limitpositions [position].minval, limitpositions[position].maxval), limitpositions[position].ypos, transform.position.z);
        this.transform.position = limits;//new Vector3(player.position.x + offset.x, this.transform.position.y, this.transform.position.z);
        if (position!=0)
        {
            HD.color = Color.black;
        }
        else
        {
            HD.color = x;
        }
    }
}
