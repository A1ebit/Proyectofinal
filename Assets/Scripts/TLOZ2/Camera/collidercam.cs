﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class collidercam : MonoBehaviour
{
    public Collider2D left;
    public Collider2D right;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag=="Player"|| collision.gameObject.tag == "Boss")
        {
            return;
        }
        else
        {
            Physics2D.IgnoreCollision(collision.collider, left);
            Physics2D.IgnoreCollision(collision.collider, right);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player" || collision.tag == "Boss")
        {
            return;
        }
        else
        {
            Physics2D.IgnoreCollision(collision, left);
            Physics2D.IgnoreCollision(collision, right);
        }
    }
}
