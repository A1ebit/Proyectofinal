﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DañoEnemyRec : MonoBehaviour
{
    Manager2 manager2;
    private Slime padre;
    private pez father;
    private DarkLink Otosan;
    private PlayerControl link;
    public bool slimme;
    public bool Boss;
    public bool pez;
    Vector3 X;
    AudioManager Audios;

    // Start is called before the first frame update
    void Start()
    {
        Audios = FindObjectOfType<AudioManager>();
        link = FindObjectOfType<PlayerControl>();
        manager2 = FindObjectOfType<Manager2>();
        if (slimme)
        {
            padre = transform.parent.GetComponent<Slime>();
        }
        if (pez)
        {
            father = transform.parent.GetComponent<pez>();
        }
        if (Boss)
        {
            Otosan = transform.parent.GetComponent<DarkLink>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        //Time.timeScale = 0.2f;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (slimme)
        {
            if (collision.name == "sword")
            {
                if (collision.GetComponentInParent<PlayerControl>().AireAtkedown == true)
                {
                    collision.GetComponentInParent<PlayerControl>().rb.velocity = Vector3.zero;
                    collision.transform.parent.gameObject.GetComponent<Rigidbody2D>().AddForce(transform.up * 15f, ForceMode2D.Impulse); //forma correcta
                    collision.GetComponentInParent<PlayerControl>().AireAtkedown = false;
                }
                else
                {
                    padre.rb.velocity = Vector3.zero;
                    if (padre.mov == -1)
                    {
                        padre.rb.AddForce(new Vector2(9f, 6f), ForceMode2D.Impulse);
                    }
                    else
                    {
                        padre.rb.AddForce(new Vector2(-9f, 6f), ForceMode2D.Impulse);
                    }
                }           
                padre.dmgrcv = true;
                StartCoroutine(padre.stocolordmgp());
                if (padre.vida>1)
                {
                    Audios.Play("SwordHit");
                }
                else
                {
                    Audios.Play("EnemyDeath");
                }
                padre.vida--;
            }
            else if (collision.tag=="PlayerDMG")
            {
                if (PlayerControl.Invencible == false)
                {
                    Audios.Play("LinkHurt");
                    manager2.TakeDamage(padre.damage);
                    Debug.Log("me has hecho daño");
                }
            }
            else if (collision.tag == "Shield")
            {
                padre.rb.velocity = new Vector2(-padre.rb.velocity.x, padre.rb.velocity.y);
            }
        }

        if (pez)
        {
            if (collision.name == "sword")
            {
                father.rb.velocity = Vector3.zero;
                if (collision.GetComponentInParent<PlayerControl>().AireAtkedown == true)
                {
                    collision.GetComponentInParent<PlayerControl>().rb.velocity = Vector3.zero;
                    collision.transform.parent.gameObject.GetComponent<Rigidbody2D>().AddForce(transform.up * 15f, ForceMode2D.Impulse); //forma correcta
                    collision.GetComponentInParent<PlayerControl>().AireAtkedown = false;
                }
                Audios.Play("EnemyDeath");
                father.vida--;
            }
            else if (collision.tag == "PlayerDMG")
            {
                if (PlayerControl.Invencible == false)
                {
                    Audios.Play("LinkHurt");
                    manager2.TakeDamage(father.damage);
                    Debug.Log("me has hecho daño");
                }
            }
        }

        if (Boss)
        {
            if (collision.name == "sword" && Otosan.invencible == false)
            {
                X = collision.transform.position;
                Otosan.nodmg = .5f;
                Otosan.dolor = true;
                if (collision.GetComponentInParent<PlayerControl>().AireAtkedown == true)
                {
                    collision.GetComponentInParent<PlayerControl>().rb.velocity = Vector3.zero;
                    collision.transform.parent.gameObject.GetComponent<Rigidbody2D>().AddForce(transform.up * 15f, ForceMode2D.Impulse); //forma correcta
                    collision.GetComponentInParent<PlayerControl>().AireAtkedown = false;
                }
                StartCoroutine(DañoActive());
            }
        }
    }

    IEnumerator DañoActive()
    {
        yield return new WaitForSeconds(0.01f);
        if (Otosan.Escudo==false)
        {
            if (Otosan.hp > 1)
            {
                Audios.Play("SwordHit");
            }
            if (Otosan.gameObject.transform.position.x < X.x)
            {
                //Debug.Log("lanzamientoenemigo");
                Otosan.rb.velocity = Vector3.zero;
                Otosan.rb.AddForce(new Vector2(-6f, 16f), ForceMode2D.Impulse);
            }
            else
            {
                Debug.Log("lanzamientoenemigo");
                Otosan.rb.velocity = Vector3.zero;
                Otosan.rb.AddForce(new Vector2(6f, 16f), ForceMode2D.Impulse);
            }
            Otosan.anim.Play("hurt");
            Otosan.anim.SetBool("damage", true);
            DarkLink.health--;
        }
    }
}
