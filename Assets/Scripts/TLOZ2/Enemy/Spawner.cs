﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public Transform[] positions;
    public Transform father;
    public GameObject pez;
    void Start()
    {
        father = GameObject.Find("Characters/Enemys/2daZona/PezContainer").transform;
        positions = new Transform[transform.childCount];
        for (int i = 0; i < transform.childCount; i++)
        {
            positions[i] = transform.GetChild(i);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void invokar()
    {
        int x = Random.Range(0, 4);
        Instantiate(pez,positions[x].position, positions[x].rotation, father);
    }
}
