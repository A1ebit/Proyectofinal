﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pez : MonoBehaviour
{
    public float Speed;
    public float JumpForce;
    public Rigidbody2D rb;
    public bool isground;
    public bool rebot = false;
    public bool lookLink = true;
    private GameObject target;
    public GameObject Proyectile;
    private Transform SpawnPoint;
    public float mov;
    public Collider2D xd;
    //[HideInInspector]
    public int shootPos;
    [HideInInspector]
    public bool shootPosCan;
    public GameObject expnum;
    public int exp;
    public Manager2 manager2;
    public float damage;
    public int vida;
    public Collider2D trigger;
    private Animator anim;
    private bool aka;

    void Start()
    {
        aka = true;
        anim = GetComponent<Animator>();
        vida = 1;
        damage = 0.7f;
        manager2 = GameObject.FindObjectOfType<Manager2>();
        exp = Random.Range(10, 35);
        xd = GameObject.Find("Grid/Collider/ColliderPez").GetComponent<Collider2D>();
        shootPosCan = true;
        shootPos = Random.Range(0,4);
        SpawnPoint = this.transform.GetChild(2).transform;
        target = GameObject.FindGameObjectWithTag("Player");
        rb = this.GetComponent<Rigidbody2D>();
        Physics2D.IgnoreCollision(xd, this.GetComponent<Collider2D>());
        StartCoroutine(inicio());
    }
    private void FixedUpdate()
    {
        Physics2D.IgnoreCollision(GameObject.FindGameObjectWithTag("Player").GetComponent<Collider2D>(), this.GetComponent<Collider2D>());
        Physics2D.IgnoreCollision(GameObject.FindObjectOfType<collidercam>().left, this.GetComponent<Collider2D>());
        Physics2D.IgnoreCollision(GameObject.FindObjectOfType<collidercam>().right, this.GetComponent<Collider2D>());
    }
    // Update is called once per frame
    void Update()
    {
        isground = this.transform.GetComponentInChildren<collidermoveenemy>().isground;
        if (isground)
        {
            if (lookLink)
            {
                movim();
                lookLink = false;
            }
            if (shootPos==0 && shootPosCan)
            {
                StartCoroutine(shoot());
            }

            else
            {
                rb.velocity = new Vector2(mov * Speed, rb.velocity.y);
            }
        }

        if (vida <= 0)
        {
            rb.bodyType = RigidbodyType2D.Kinematic;
            rb.velocity = Vector3.zero;
            Death();
        }
    }

    void Death()
    {
        trigger.enabled = false;
        this.tag = "Untagged";
        if (aka)
        {
            StartCoroutine(onDeath());
        }
        trigger.enabled = false;
        anim.Play("death");
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag=="Enemy")
        {
            Physics2D.IgnoreCollision(collision.collider, this.GetComponent<Collider2D>());
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "ColliderPez")
        {
            Debug.Log("el Gato Volador");
            StartCoroutine(stopignore());
        }

        if (collision.tag == "Enemy")
        {
            Physics2D.IgnoreCollision(collision, this.GetComponent<Collider2D>());
        }
    }

    void movim()
    {
        if (transform.position.x > target.transform.position.x)
        {
            mov = -1;
            this.transform.localScale = new Vector3(1f, transform.localScale.y, transform.localScale.z);
        }
        else if (transform.position.x < target.transform.position.x)
        {
            mov = 1;
            this.transform.localScale = new Vector3(-1f, transform.localScale.y, transform.localScale.z);
        }
        Speed = 4f;
    }
    IEnumerator inicio()
    {
        int x = Random.Range(0,2);
        switch (x)
        {
            case 0:
            mov = 1;          
                this.transform.localScale = new Vector3(-1f, transform.localScale.y, transform.localScale.z);
                break; 
            case 1:
            mov = -1;
                this.transform.localScale = new Vector3(1f, transform.localScale.y, transform.localScale.z);
                break;
        }
        yield return new WaitForSeconds(0.3f);
        gameObject.transform.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
        rb.AddForce(new Vector2(mov * Speed, JumpForce), ForceMode2D.Impulse);
    }
    IEnumerator stopignore()
    {
        yield return new WaitForSeconds(0.2f);
        rebot = true;
        this.transform.GetChild(0).gameObject.GetComponent<Collider2D>().enabled = true;
        Physics2D.IgnoreCollision(xd, this.GetComponent<Collider2D>(),false);
        //this.GetComponentInChildren<Collider2D>().enabled = true;   //pq no funciona
    }

    IEnumerator shoot()
    {
        shootPosCan = false;
        yield return new WaitForSeconds(1.5f);
        if (mov==-1)
        {
            GameObject X = Instantiate(Proyectile, SpawnPoint.position, SpawnPoint.rotation, this.gameObject.transform.parent.transform);
            Destroy(X, 5f);
        }
        else
        {
            GameObject X = Instantiate(Proyectile, SpawnPoint.position, Quaternion.Euler(SpawnPoint.rotation.x,180f, SpawnPoint.rotation.z), this.gameObject.transform.parent.transform);
            Destroy(X, 5f);
        }
        Proyectile.transform.localScale = Vector3.one;
    }

    IEnumerator onDeath()
    {
        aka = false;
        yield return new WaitForSeconds(0.6f);
        expnum.GetComponent<TextExp>().ExpInstance(exp);
        FindObjectOfType<AudioManager>().Play("EXP");
        Instantiate(expnum, new Vector3(transform.position.x + Random.Range(-1f, 1f), transform.position.y + Random.Range(-1f, 1f), this.transform.position.z), this.transform.rotation, GameObject.Find("Exp_Mesagge").transform);
        yield return new WaitForSeconds(0.05f);
        Destroy(this.gameObject);
    }
}
