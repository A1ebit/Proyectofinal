﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bote : MonoBehaviour
{
    public bool fish;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (fish)
        {
            if (this.transform.parent.GetComponent<pez>().isground == false && this.transform.parent.GetComponent<pez>().rebot == true)
            {
                Rigidbody2D x = this.transform.GetComponentInParent<Rigidbody2D>();
                x.velocity = new Vector2(-x.velocity.x, x.velocity.y);
                this.transform.parent.gameObject.transform.localScale = new Vector3((-1*transform.parent.gameObject.transform.localScale.x), transform.parent.gameObject.transform.localScale.y, transform.parent.gameObject.transform.localScale.z);
            }
        }
        else
        {
            Rigidbody2D x = this.transform.GetComponentInParent<Rigidbody2D>();
            x.velocity = new Vector2(-x.velocity.x, x.velocity.y);
        }

    }    
    private void OnTriggerExit2D(Collider2D collision)
    {

    }
}
