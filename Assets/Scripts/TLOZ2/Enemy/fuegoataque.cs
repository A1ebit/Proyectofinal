﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class fuegoataque : MonoBehaviour
{
    public float speed;
    public Rigidbody2D rb;
    public float damage;
    private Manager2 manager2;
    private AudioSource audie;

    void Start()
    {
        FindObjectOfType<AudioManager>().Play("Fire");
        manager2 = GameObject.FindObjectOfType<Manager2>();
        damage = 0.5f;
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = -transform.right * speed;
        audie = this.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (manager2._IsPaused)
        {
            audie.Pause();
        }
        else
        {
            if (audie.isPlaying==false)
            {
                audie.Play();
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "PlayerDMG")
        {
            audie.Stop();
            if (PlayerControl.Invencible == false)
            {
                FindObjectOfType<AudioManager>().Play("LinkHurt");
                manager2.TakeDamage(damage);
                Debug.Log("me has hecho daño");
            }
            Destroy(this.gameObject);
        }
        else if (collision.tag=="Shield")
        {
            audie.Stop();
            FindObjectOfType<AudioManager>().Stop("Fire");
            FindObjectOfType<AudioManager>().Play("ShieldDeflect");
            Destroy(this.gameObject);
        }
    }
}
