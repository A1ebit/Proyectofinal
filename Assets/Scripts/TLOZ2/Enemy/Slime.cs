﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slime : MonoBehaviour
{
    public float Speed;
    public float JumpForce;
    public Rigidbody2D rb;
    public float mov;
    public float timejump;
    public float timeforjump;
    public Animator anim;
    public bool isground;
    public LayerMask ground;
    public GameObject target;
    public GameObject expnum;
    public float damage;
    public int exp;
    public int vida;
    public BoxCollider2D trigger;
    private SpriteRenderer slime;
    private bool cl;
    public bool dmgrcv;

    private void Start()
    {
        cl = true;
        slime = GetComponent<SpriteRenderer>();
        damage = 0.5f;
        target = GameObject.FindGameObjectWithTag("Player");
        anim = this.GetComponent<Animator>();
        rb = this.GetComponent<Rigidbody2D>();
        exp = Random.Range(1, 20);
        vida = 2;
    }

    private void FixedUpdate()
    {
        Physics2D.IgnoreCollision(GameObject.FindGameObjectWithTag("Player").GetComponent<Collider2D>(), this.GetComponent<Collider2D>());
        Physics2D.IgnoreCollision(GameObject.FindObjectOfType<collidercam>().left, this.GetComponent<Collider2D>());
        Physics2D.IgnoreCollision(GameObject.FindObjectOfType<collidercam>().right, this.GetComponent<Collider2D>());
    }
    private void Update()
    {
        isground = this.transform.GetComponentInChildren<collidermoveenemy>().isground;

        if (transform.position.x > target.transform.position.x)
        {
            mov=-1;
        }
        else
        {
            mov = 1;
        }

        if (isground)
        {
            anim.SetBool("piso", true);
        }
        else
        {
            anim.SetBool("piso", false);
        }

        if (vida<=0)
        {
            rb.velocity = Vector3.zero;
            Death();
        }
        else
        {
            movim();
        }
    }

    void Jumping()
    {
        anim.Play("Aire");
        rb.velocity = new Vector2(mov * Speed, JumpForce);
    }

    void movim()
    {
        if (isground)
        {
            timejump += Time.deltaTime;
            if (timejump>timeforjump)
            {
                timejump = 0;
                Jumping();
            }
        }
        else if (isground && rb.velocity.y < 0.01f)
        {
            rb.velocity = Vector3.zero;

        }

        /*else if (Input.GetKey(KeyCode.S) && isground && rb.velocity.y <= 0.001f)
        {
                rb.velocity = Vector3.zero;
        }
        else
        {
            rb.velocity = new Vector2(mov * Speed, rb.velocity.y);
        }*/

    }

    void Death()
    {
        trigger.enabled = false;
        this.tag = "Untagged";
        anim.SetBool("Death", true);
        rb.bodyType = RigidbodyType2D.Kinematic;
        if (cl)
        {
            StartCoroutine(onDeath());
        }
        trigger.enabled = false;
        anim.Play("death");
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            Physics2D.IgnoreCollision(collision.collider, this.GetComponent<Collider2D>());
        }
    }
    IEnumerator onDeath()
    {
        cl = false;
        yield return new WaitForSeconds(0.6f);
        FindObjectOfType<AudioManager>().Play("EXP");
        expnum.GetComponent<TextExp>().ExpInstance(exp);
        Instantiate(expnum, new Vector3(transform.position.x + Random.Range(-1f, 1f), transform.position.y + Random.Range(-1f, 1f), this.transform.position.z), this.transform.rotation,GameObject.Find("Exp_Mesagge").transform);
        yield return new WaitForSeconds(0.05f);
        Destroy(this.gameObject);
    }

    public IEnumerator stocolordmgp()
    {
        if (vida>1)
        {
            int xd = 0;
            while (dmgrcv==true)
            {
                timejump = 0;
                yield return new WaitForSeconds(0.15f);
                timejump = 0;
                slime.color = Color.blue;//new Color(1f, 1f, 1f, 0f);
                yield return new WaitForSeconds(0.15f);
                timejump = 0;
                slime.color = new Color(1f, 1f, 1f, 1f);
                xd++;
                if (xd>4)
                {
                    dmgrcv = false;
                }
            }
        }

    }
}
