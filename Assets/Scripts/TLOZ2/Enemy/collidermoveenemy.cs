﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class collidermoveenemy : MonoBehaviour
{
    public bool isground;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 8)
        {
            isground = true;
        }
    }    
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 8)
        {
            isground = false;
        }
    }
}
