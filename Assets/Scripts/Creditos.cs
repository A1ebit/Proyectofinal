﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Creditos : MonoBehaviour
{
    [SerializeField]
    private Text credits;    
    [SerializeField]
    private GameObject end;
    public Color color;
    [SerializeField]
    AudioSource music;
    void Start()
    {
        StartCoroutine(aparecer(credits, color, 2f));
        StartCoroutine(Sonido(music, 3.5f,0.02f));
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator aparecer(Text tex, Color color, float speed)
    {
        yield return new WaitForSeconds(.8f);
        while (tex.color != color)
        {
            tex.color = Color.Lerp(tex.color, color, Time.deltaTime * speed);
            yield return null;
        }
        credits.GetComponentInParent<Animator>().Play("Mover");
        yield return new WaitForSeconds(18.5f);
        end.SetActive(true);
        yield return new WaitForSeconds(4.7f);
        Debug.LogWarning("werwrw");
        music.Stop();
        yield return new WaitForSeconds(0.1f);
        Application.Quit();
    }
    IEnumerator Sonido(AudioSource audioSource, float duration, float targetVolume)
    {
        {
            float currentTime = 0;
            float start = audioSource.volume;

            while (currentTime < duration)
            {
                currentTime += Time.deltaTime;
                audioSource.volume = Mathf.Lerp(start, targetVolume, currentTime / duration);
                yield return null;
            }
            yield break;
        }
    }
}
