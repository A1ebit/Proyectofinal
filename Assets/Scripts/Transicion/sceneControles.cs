﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class sceneControles : MonoBehaviour
{
    private static sceneControles _instance;
    private static int NextScene;
    public static int _NextScene;
    //controles de la scena numero
    private static int NextSceneControlles;
    public static int _NextSceneControlles;
    [SerializeField]
    private int scene;    
    [SerializeField]
    private int controlles;
    public static sceneControles Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("sceneControles : Error");
            }
            return _instance;
        }
    }
    void Start()
    {
        if (_instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }
    void Update()
    {
        controlles = NextSceneControlles;
        scene = NextScene;

        /*if (Input.GetKeyDown(KeyCode.Alpha0))
        {
            SceneManager.LoadScene(0);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            SceneManager.LoadScene(1);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            SceneManager.LoadScene(2);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            SceneManager.LoadScene(3);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            SceneManager.LoadScene(4);
        }*/
    }
    public static void NextSceneNum(int SceneNum)
    {
        NextScene = SceneNum;
        _NextScene = NextScene;
    }  
    public static void NextSceneControllesNum(int SceneNum)
    {
        NextSceneControlles = SceneNum;
        _NextSceneControlles = NextSceneControlles;
    }    
    public static void ChangeScene()
    {
        SceneManager.LoadScene(NextScene);
    }   
    public static void ChangeSceneControlGame()
    {
        SceneManager.LoadScene(NextSceneControlles);
    }
}
