﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TypeWrittereffect : MonoBehaviour
{
    Text texto;
    [SerializeField]
    private Font fuente_nueva;
    string textosave;
    [SerializeField]
    private float wait;
    bool start=true;

    // Start is called before the first frame update
    void Start()
    {
        texto = this.GetComponent<Text>();
        textosave = texto.text;
    }

    // Update is called once per frame
    void Update()
    {
        if (start)
        {
            wait += Time.deltaTime;
        }

        if (wait>3.2f)
        {
            texto.enabled = false;
            start = false;
            wait = 0;
            StartCoroutine(escribir());
        }
    }
    IEnumerator escribir()
    {
        texto.font = fuente_nueva;
        texto.text = "";
        yield return new WaitForSeconds(0.15f);
        texto.enabled = true;
        foreach (char letter in textosave.ToCharArray())
        {
            texto.text += letter;
            yield return new WaitForSeconds(0.15f);
        }
    }

}
