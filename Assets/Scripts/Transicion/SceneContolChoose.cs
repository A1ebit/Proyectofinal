﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneContolChoose : MonoBehaviour
{
    [SerializeField]
    private GameObject[] _SceneContol;    
    [SerializeField]
    private GameObject[] Tloz2;    
    [SerializeField]
    private float timefade;   
    [SerializeField]
    private Animator _Fade;
    private bool idk=true;
    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        _SceneContol = new GameObject[this.gameObject.transform.childCount];
        for (int i = 0; i < _SceneContol.Length; i++)
        {
            _SceneContol[i] = transform.GetChild(i).gameObject;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            sceneControles.ChangeSceneControlGame();
        }

        switch (sceneControles._NextSceneControlles)
        {
            case 1:_SceneContol[0].SetActive(true);
                if (idk)
                {
                    timefade += Time.deltaTime;
                }
                if (timefade>=8.2)
                {
                    idk = false;
                    timefade = 0;
                    _Fade.Play("Fade_out");
                    StartCoroutine(change());
                }
                break;
            case 2:_SceneContol[1].SetActive(true);
                if (idk)
                {
                    timefade += Time.deltaTime;
                }
                if (timefade >= 8.2)
                {
                    idk = false;
                    timefade = 0;
                    _Fade.Play("Fade_out");
                    _Fade.transform.GetChild(0).gameObject.SetActive(true);
                    StartCoroutine(changeP1_P2());
                }
                break;
            case 3:
                _SceneContol[2].SetActive(true);
                if (idk)
                {
                    timefade += Time.deltaTime;
                }
                if (timefade >= 8.2)
                {
                    idk = false;
                    timefade = 0;
                    _Fade.Play("Fade_out");
                    StartCoroutine(change());
                }
                break;
        }
    }

    IEnumerator change()
    {
        yield return new WaitForSeconds(1.3f);
        sceneControles.ChangeSceneControlGame();
    }   
    IEnumerator changeP1_P2()
    {
        yield return new WaitForSeconds(1f);
        Tloz2[0].SetActive(false);
        Tloz2[1].SetActive(true);
        _Fade.Play("FadeIn");
        yield return new WaitForSeconds(4.5f);
        _Fade.transform.GetChild(0).gameObject.SetActive(false);
        _Fade.Play("Fade_out");
        yield return new WaitForSeconds(1.6f);
        sceneControles.ChangeSceneControlGame();
        Tloz2[0].SetActive(true);
    }

}
