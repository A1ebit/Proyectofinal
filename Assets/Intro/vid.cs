﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Video;

public class vid : MonoBehaviour
{
    // Start is called before the first frame update
    public VideoPlayer videoIntro;
    private bool inico = true;
    private  bool espada;
    private  bool tey;
    public GameObject ad;
    public GameObject ad1;
    public GameObject dc;
    public GameObject sword;
    public GameObject trans;
    public GameObject skip;
    private Vector3 swordLast;
    public GameObject check;
    public GameObject textoInc;
    [HideInInspector]
    public Font tri;
    [HideInInspector]
    public Font sheik;
    public float reset;
    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        swordLast = sword.transform.position;
        dc.SetActive(false);
        ad1.SetActive(false);
        textoInc.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (videoIntro.enabled)
        {
            videoIntro.loopPointReached += EndReached;
        }
        if (videoIntro.enabled==false && inico)
        {
            StartCoroutine("trueno");
        }
        if (espada)
        {
            sword.transform.position = Vector3.MoveTowards(sword.transform.position, check.transform.position, 15f);
            if (Vector3.Distance(sword.transform.position,check.transform.position)<0.01f)
            {
                espada = false;
                tey = true;
                textoInc.SetActive(true);
                StartCoroutine("letras");
            }
        }
        if (Input.anyKeyDown && videoIntro.isPlaying)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                videoIntro.enabled = false;
                StartCoroutine("trueno");
            }
            else
            {
                StartCoroutine(skipbtoon());
            }
        }

        if (tey==true)
        {
            reset += Time.deltaTime;
            if (Input.anyKeyDown && videoIntro.enabled == false)
            {
                textoInc.GetComponent<AudioSource>().Play();
                tey = false;
                StartCoroutine("newscene");
                Debug.Log("comienza los madrazos");
            }
        }

        if (reset>=25f)
        {
            StartCoroutine("resetsoun");
        }
    }

    void ReinicioIn()
    {
        reset = 0f;
        dc.SetActive(false);
        ad1.SetActive(false);
        textoInc.SetActive(false);
        sword.transform.position = swordLast;
        videoIntro.enabled = true;
        tey = false;
        inico = true;
    }

    void EndReached(UnityEngine.Video.VideoPlayer vp)
    {
        vp.enabled = false;
        skip.SetActive(false);
    }

    IEnumerator trueno()
    {
        this.GetComponent<AudioSource>().Play();
        this.GetComponent<Animator>().Play("idle");
        inico = false;
        espada = true;
        for (int i = 0; i < 4; i++)
        {
            ad.SetActive(true);
            yield return new WaitForSeconds(0.1f);
            ad.SetActive(false);
            yield return new WaitForSeconds(0.1f);
        }
        dc.SetActive(true);
        ad1.SetActive(true);
    }  

    IEnumerator resetsoun()
    {
        this.GetComponent<Animator>().Play("fade");
        yield return new WaitForSeconds(0.38f);
        ReinicioIn();
    }    
    IEnumerator newscene()
    {
        this.GetComponent<Animator>().Play("fade");
        trans.SetActive(true);
        yield return new WaitForSeconds(0.70f);
        sceneControles.NextSceneNum(4);
        sceneControles.NextSceneControllesNum(1);
        yield return new WaitForSeconds(0.05f);
        sceneControles.ChangeScene();
        //SceneManager.LoadScene(1);
    }

    IEnumerator letras()
    {
        while (tey==true)
        {
            yield return new WaitForSeconds(0.3f);
            textoInc.GetComponent<Text>().enabled = false;
            yield return new WaitForSeconds(0.2f);
            textoInc.GetComponent<Text>().font = sheik;
            textoInc.GetComponent<Text>().enabled = true;
            yield return new WaitForSeconds(0.3f);
            textoInc.GetComponent<Text>().enabled = false;
            yield return new WaitForSeconds(0.2f);
            textoInc.GetComponent<Text>().font = tri;
            textoInc.GetComponent<Text>().enabled = true;
            yield return new WaitForSeconds(0.3f);
        }

      /*  for (int i = 0; i < Mathf.Infinity; i++)
        {
            textoInc.GetComponent<Text>().font = tri;
            yield return new WaitForSeconds(1f);
            textoInc.GetComponent<Text>().enabled = false;
            yield return new WaitForSeconds(1f);    
            textoInc.GetComponent<Text>().enabled = true;
            yield return new WaitForSeconds(1f);
            textoInc.GetComponent<Text>().font = sheik;
            yield return new WaitForSeconds(1f);
            textoInc.GetComponent<Text>().enabled = false;
            yield return new WaitForSeconds(1f);
            textoInc.GetComponent<Text>().enabled = true;
            yield return new WaitForSeconds(1f);
        }*/
    }    
    IEnumerator skipbtoon()
    {
        skip.SetActive(true);
        yield return new WaitForSeconds(0.85f);
        skip.SetActive(false);
    }
}
